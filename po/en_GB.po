# English (British) translation.
# Copyright (C) 2004 THE straw's COPYRIGHT HOLDER
# This file is distributed under the same license as the straw package.
# Gareth Owen <gowen72@yahoo.com>, 2004.
#
#
msgid ""
msgstr ""
"Project-Id-Version: straw\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-05-16 08:51+0100\n"
"PO-Revision-Date: 2007-05-16 08:51-0000\n"
"Last-Translator: David Lodge <dave@cirt.net>\n"
"Language-Team:  <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../glade/straw.glade.h:1
msgid " "
msgstr " "

#: ../glade/straw.glade.h:2
msgid "    "
msgstr "    "

#: ../glade/straw.glade.h:3
msgid "0 items found"
msgstr "0 items found"

#: ../glade/straw.glade.h:4
msgid "<b>Global</b>"
msgstr "<b>Global</b>"

#: ../glade/straw.glade.h:5
msgid "<b>Web Browser</b>"
msgstr "<b>Web Browser</b>"

#: ../glade/straw.glade.h:7
#, no-c-format
msgid "<small><i>(example: <b>epiphany %s</b>)</i></small>"
msgstr "<small><i>(example: <b>epiphany %s</b>)</i></small>"

#: ../glade/straw.glade.h:8
msgid ""
"<span weight=\"bold\">C_ategories\n"
"</span>"
msgstr ""
"<span weight=\"bold\">C_ategories\n"
"</span>"

#: ../glade/straw.glade.h:10
msgid "<span weight=\"bold\">_External Source for Category Contents:</span>"
msgstr "<span weight=\"bold\">_External Source for Category Contents:</span>"

#: ../glade/straw.glade.h:11
msgid "<span weight=\"bold\">_Sort Category Contents:</span>"
msgstr "<span weight=\"bold\">_Sort Category Contents:</span>"

#: ../glade/straw.glade.h:12
msgid "Add a new feed to the subscription list"
msgstr "Add a new feed to the subscription list"

#: ../glade/straw.glade.h:13
msgid "Earliest date"
msgstr "Earliest date"

#: ../glade/straw.glade.h:14
msgid "Feed Copyright"
msgstr "Feed Copyright"

#: ../glade/straw.glade.h:15
msgid "Feed Description"
msgstr "Feed Description"

#: ../glade/straw.glade.h:16
msgid "Feed _Location:"
msgstr "Feed _Location:"

#: ../glade/straw.glade.h:17
msgid "If the feed requires authentication, please enter them below."
msgstr "If the feed requires authentication, please enter them below."

#: ../glade/straw.glade.h:18
msgid "Latest date"
msgstr "Latest date"

#: ../glade/straw.glade.h:19
msgid "Location:"
msgstr "Location:"

#: ../glade/straw.glade.h:20
msgid "Mark Feed as _Read"
msgstr "Mark Feed as _Read"

#: ../glade/straw.glade.h:21
msgid "Mark _All Feeds As Read"
msgstr "Mark _All Feeds As Read"

#: ../glade/straw.glade.h:22
msgid "N_ext Feed"
msgstr "N_ext Feed"

#: ../glade/straw.glade.h:23
msgid "Ne_xt Feed with Unread"
msgstr "Ne_xt Feed with Unread"

#: ../glade/straw.glade.h:24
msgid "Next Refresh:"
msgstr "Next Refresh:"

#: ../glade/straw.glade.h:25
msgid "Next _Category"
msgstr "Next _Category"

#: ../glade/straw.glade.h:26
msgid "P_revious Feed"
msgstr "P_revious Feed"

#: ../glade/straw.glade.h:27
msgid "Password:"
msgstr "Password:"

#: ../glade/straw.glade.h:28
msgid "Previous Ca_tegory"
msgstr "Previous Ca_tegory"

#: ../glade/straw.glade.h:29
msgid "Previous Refresh:"
msgstr "Previous Refresh:"

#: ../glade/straw.glade.h:30
msgid "Re_move Selected Feed"
msgstr "Re_move Selected Feed"

#: ../glade/straw.glade.h:31
msgid "Refresh _All"
msgstr "Refresh _All"

#: ../glade/straw.glade.h:32
msgid "Refresh _Category"
msgstr "Refresh _Category"

#: ../glade/straw.glade.h:33
msgid "Refresh _Selected"
msgstr "Refresh _Selected"

#: ../glade/straw.glade.h:34
msgid "Refresh all feeds"
msgstr "Refresh all feeds"

#: ../glade/straw.glade.h:35
msgid "Refresh frequency:"
msgstr "Refresh frequency:"

#: ../glade/straw.glade.h:36
msgid "Report a _problem"
msgstr "Report a _problem"

#: ../glade/straw.glade.h:37
msgid "Return to feed list"
msgstr "Return to feed list"

#: ../glade/straw.glade.h:38
msgid "Scroll or Next _Unread Item"
msgstr "Scroll or Next _Unread Item"

#: ../glade/straw.glade.h:39
msgid "Scroll to the next unread article"
msgstr "Scroll to the next unread article"

#: ../glade/straw.glade.h:40
msgid "Straw"
msgstr "Straw"

#: ../glade/straw.glade.h:41
msgid "Straw - Subscribe to Feed"
msgstr "Straw - Subscribe to Feed"

#: ../glade/straw.glade.h:42
msgid "Straw Preferences"
msgstr "Straw Preferences"

#: ../glade/straw.glade.h:43
msgid "Subscription _Information"
msgstr "Subscription _Information"

#: ../glade/straw.glade.h:44
msgid "Us_e Default"
msgstr "Us_e Default"

#: ../glade/straw.glade.h:45
msgid "Use _Default"
msgstr "Use _Default"

#: ../glade/straw.glade.h:46
msgid "User name:"
msgstr "User name:"

#: ../glade/straw.glade.h:47
msgid "_Categories"
msgstr "_Categories"

#: ../glade/straw.glade.h:48
msgid "_Date Filter"
msgstr "_Date Filter"

#: ../glade/straw.glade.h:49
msgid "_Display location of new items:"
msgstr "_Display location of new items:"

#: ../glade/straw.glade.h:50
msgid "_Edit"
msgstr "_Edit"

#: ../glade/straw.glade.h:51
msgid "_Export Subscriptions"
msgstr "_Export Subscriptions"

#: ../glade/straw.glade.h:52
msgid "_File"
msgstr "_File"

#: ../glade/straw.glade.h:53
msgid "_General"
msgstr "_General"

#: ../glade/straw.glade.h:54
msgid "_General Settings"
msgstr "_General Settings"

#: ../glade/straw.glade.h:55
msgid "_Go"
msgstr "_Go"

#: ../glade/straw.glade.h:56
msgid "_Help"
msgstr "_Help"

#: ../glade/straw.glade.h:57
msgid "_Import Subscriptions"
msgstr "_Import Subscriptions"

#: ../glade/straw.glade.h:58
msgid "_Location:"
msgstr "_Location:"

#: ../glade/straw.glade.h:59
msgid "_Name:"
msgstr "_Name:"

#: ../glade/straw.glade.h:60
msgid "_Next Item"
msgstr "_Next Item"

#: ../glade/straw.glade.h:61
msgid "_Next Refresh:"
msgstr "_Next Refresh:"

#: ../glade/straw.glade.h:62
msgid "_Number of articles per feed:"
msgstr "_Number of articles per feed:"

#: ../glade/straw.glade.h:63
msgid "_Number of articles to store:"
msgstr "_Number of articles to store:"

#: ../glade/straw.glade.h:64
msgid "_Override preferred browser setting:"
msgstr "_Override preferred browser setting:"

#: ../glade/straw.glade.h:65
msgid "_Password:"
msgstr "_Password:"

#: ../glade/straw.glade.h:66
msgid "_Previous Item"
msgstr "_Previous Item"

#: ../glade/straw.glade.h:67
msgid "_Refresh & Storage"
msgstr "_Refresh & Storage"

#: ../glade/straw.glade.h:68
msgid "_Refresh frequency (minutes):"
msgstr "_Refresh frequency (minutes):"

#: ../glade/straw.glade.h:69
msgid "_Refresh frequency:"
msgstr "_Refresh frequency:"

#: ../glade/straw.glade.h:70
msgid "_Search items for:"
msgstr "_Search items for:"

#: ../glade/straw.glade.h:71
msgid "_Subscribe to New Feed"
msgstr "_Subscribe to New Feed"

#: ../glade/straw.glade.h:72
msgid "_Subscriptions:"
msgstr "_Subscriptions:"

#: ../glade/straw.glade.h:73
msgid "_Username & Password"
msgstr "_Username & Password"

#: ../glade/straw.glade.h:74
msgid "_Username:"
msgstr "_Username:"

#: ../glade/straw.glade.h:75
msgid "_View"
msgstr "_View"

#: ../glade/straw.glade.h:76
msgid "_Visit the web site at "
msgstr "_Visit the web site at "

#: ../glade/straw.glade.h:77
msgid "_bottom of the list"
msgstr "_bottom of the list"

#: ../glade/straw.glade.h:78
msgid "_top of the list"
msgstr "_top of the list"

#: ../glade/straw.glade.h:79
msgid "gtk-about"
msgstr "gtk-about"

#: ../glade/straw.glade.h:80
msgid "gtk-close"
msgstr "gtk-close"

#: ../glade/straw.glade.h:81
msgid "gtk-copy"
msgstr "gtk-copy"

#: ../glade/straw.glade.h:82
msgid "gtk-find"
msgstr "gtk-find"

#: ../glade/straw.glade.h:83
msgid "gtk-ok"
msgstr "gtk-ok"

#: ../glade/straw.glade.h:84
msgid "gtk-preferences"
msgstr "gtk-preferences"

#: ../glade/straw.glade.h:85
msgid "gtk-quit"
msgstr "gtk-quit"

#: ../src/lib/Application.py:99
msgid "Category error:"
msgstr "Category error:"

#: ../src/lib/Application.py:102
msgid "Feed Error:"
msgstr "Feed Error:"

#: ../src/lib/Application.py:153
#, python-format
msgid "Next Refresh: %s"
msgstr "Next Refresh: %s"

#: ../src/lib/Application.py:155
#, python-format
msgid "%s"
msgstr "%s"

#: ../src/lib/Application.py:601
#, python-format
msgid "%(uritems)d unread in %(urfeeds)d %(fstring)s"
msgstr "%(uritems)d unread in %(urfeeds)d %(fstring)s"

#: ../src/lib/Application.py:703
msgid "Return to feed list..."
msgstr "Return to feed list..."

#: ../src/lib/Application.py:819
msgid "There was a problem while converting the database."
msgstr "There was a problem while converting the database."

#: ../src/lib/Application.py:820
msgid "Straw will not behave as expected. You should probably quit now. "
msgstr "Straw will not behave as expected. You should probably quit now. "

#: ../src/lib/Application.py:821
#, python-format
msgid "The exception has been saved to the file '%s'. Please see the Straw README for further instructions."
msgstr "The exception has been saved to the file '%s'. Please see the Straw README for further instructions."

#: ../src/lib/dialogs.py:78
msgid "Export Subscriptions"
msgstr "Export Subscriptions"

#: ../src/lib/dialogs.py:80
msgid "Select category to export:"
msgstr "Select category to export:"

#: ../src/lib/dialogs.py:100
msgid "Import Subscriptions"
msgstr "Import Subscriptions"

#: ../src/lib/dialogs.py:102
msgid "Add new subscriptions in:"
msgstr "Add new subscriptions in:"

#: ../src/lib/dialogs.py:104
msgid "OPML Files Only"
msgstr "OPML Files Only"

#: ../src/lib/dialogs.py:133
msgid "You are currently reading offline. Would you like to go online now?"
msgstr "You are currently reading offline. Would you like to go online now?"

#: ../src/lib/dialogs.py:151
msgid "A feed aggregator for the GNOME Desktop"
msgstr "A feed aggregator for the GNOME Desktop"

#. have to define this here so the titles can be translated
#: ../src/lib/FeedCategoryList.py:44
msgid "All"
msgstr "All"

#: ../src/lib/FeedCategoryList.py:45
msgid "Uncategorized"
msgstr "Uncategorised"

#: ../src/lib/FeedListView.py:102
msgid "_Refresh"
msgstr "_Refresh"

#: ../src/lib/FeedListView.py:103
msgid "Update this feed"
msgstr "Update this feed"

#: ../src/lib/FeedListView.py:104
msgid "_Mark As Read"
msgstr "_Mark As Read"

#: ../src/lib/FeedListView.py:105
msgid "Mark all items in this feed as read"
msgstr "Mark all items in this feed as read"

#: ../src/lib/FeedListView.py:106
msgid "_Stop Refresh"
msgstr "_Stop Refresh"

#: ../src/lib/FeedListView.py:107
msgid "Stop updating this feed"
msgstr "Stop updating this feed"

#: ../src/lib/FeedListView.py:108
msgid "Remo_ve Feed"
msgstr "Remo_ve Feed"

#: ../src/lib/FeedListView.py:109
msgid "Remove this feed from my subscription"
msgstr "Remove this feed from my subscription"

#: ../src/lib/FeedListView.py:110
msgid "_Arrange Feeds"
msgstr "_Arrange Feeds"

#: ../src/lib/FeedListView.py:111
msgid "Sort the current category"
msgstr "Sort the current category"

#: ../src/lib/FeedListView.py:112
msgid "Alpha_betical Order"
msgstr "Alpha_betical Order"

#: ../src/lib/FeedListView.py:113
msgid "Sort in alphabetical order"
msgstr "Sort in alphabetical order"

#: ../src/lib/FeedListView.py:114
msgid "Re_verse Order"
msgstr "Re_verse Order"

#: ../src/lib/FeedListView.py:115
msgid "Sort in reverse order"
msgstr "Sort in reverse order"

#: ../src/lib/FeedListView.py:116
msgid "_Information"
msgstr "_Information"

#: ../src/lib/FeedListView.py:117
msgid "Feed-specific properties"
msgstr "Feed-specific properties"

#: ../src/lib/FeedListView.py:163
#, python-format
msgid "Delete %s?"
msgstr "Delete %s?"

#: ../src/lib/FeedListView.py:164
#, python-format
msgid "Deleting %s will remove it from your subscription list."
msgstr "Deleting %s will remove it from your subscription list."

#: ../src/lib/FeedPropertiesDialog.py:73
msgid "Member"
msgstr "Member"

#: ../src/lib/FeedPropertiesDialog.py:77
msgid "Name"
msgstr "Name"

#: ../src/lib/FeedPropertiesDialog.py:121
#, python-format
msgid "%s Properties"
msgstr "%s Properties"

#: ../src/lib/Find.py:344
#, python-format
msgid "%d items found"
msgstr "%d items found"

#: ../src/lib/ItemList.py:145
msgid "Keep"
msgstr "Keep"

#: ../src/lib/ItemList.py:153
msgid "_Title"
msgstr "_Title"

#: ../src/lib/ItemView.py:290
msgid "Error Loading Browser"
msgstr "Error Loading Browser"

#: ../src/lib/ItemView.py:291
msgid "Please check your browser settings and try again."
msgstr "Please check your browser settings and try again."

#: ../src/lib/ItemView.py:383
msgid "Publication"
msgstr "Publication"

#: ../src/lib/ItemView.py:386
msgid "Volume"
msgstr "Volume"

#: ../src/lib/ItemView.py:391
msgid "Section"
msgstr "Section"

#: ../src/lib/ItemView.py:394
msgid "Starting Page"
msgstr "Starting Page"

#: ../src/lib/ItemView.py:402
msgid "Software license"
msgstr "Software licence"

#: ../src/lib/ItemView.py:405
msgid "Changes"
msgstr "Changes"

#: ../src/lib/ItemView.py:418
msgid "Enclosed Media"
msgstr "Enclosed Media"

#: ../src/lib/ItemView.py:421
msgid "bytes"
msgstr "bytes"

#: ../src/lib/ItemView.py:423
msgid "KB"
msgstr "KB"

#: ../src/lib/ItemView.py:426
msgid "MB"
msgstr "MB"

#: ../src/lib/ItemView.py:456
msgid "Posted by"
msgstr "Posted by"

#: ../src/lib/ItemView.py:460
msgid "Contributor:"
msgstr "Contributor:"

#: ../src/lib/ItemView.py:465
msgid "Item Source"
msgstr "Item Source"

#: ../src/lib/ItemView.py:468
msgid "Permalink"
msgstr "Permalink"

#: ../src/lib/ItemView.py:474
msgid "Complete story"
msgstr "Complete story"

#: ../src/lib/ItemView.py:479
msgid "License"
msgstr "Licence"

#: ../src/lib/ItemView.py:482
msgid "Additional information"
msgstr "Additional information"

#: ../src/lib/ItemStore.py:385
msgid "Couldn't import mx.DateTime"
msgstr "Couldn't import mx.DateTime"

#: ../src/lib/OfflineToggle.py:60
msgid "Straw is currently online. Click to go offline."
msgstr "Straw is currently online. Click to go offline."

#: ../src/lib/OfflineToggle.py:61
msgid "Straw is currently offline. Click to go online."
msgstr "Straw is currently offline. Click to go online."

#: ../src/lib/PollManager.py:214
msgid "No data"
msgstr "No data"

#: ../src/lib/PollManager.py:219
#, python-format
msgid "Unable to find the feed (%s: %s)"
msgstr "Unable to find the feed (%s: %s)"

#: ../src/lib/PollManager.py:222
#: ../src/lib/PollManager.py:303
msgid "Invalid username and password."
msgstr "Invalid username and password."

#: ../src/lib/PollManager.py:224
#, python-format
msgid "Updating feed resulted in abnormal status '%s' (code %d)"
msgstr "Updating feed resulted in abnormal status '%s' (code %d)"

#: ../src/lib/PollManager.py:235
#, python-format
msgid "An error occurred while processing feed: %s"
msgstr "An error occurred while processing feed: %s"

#: ../src/lib/PollManager.py:239
#, python-format
msgid "Updating %s done."
msgstr "Updating %s done."

#: ../src/lib/PollManager.py:252
#, python-format
msgid "Updating %s failed"
msgstr "Updating %s failed"

#: ../src/lib/PollManager.py:298
#, python-format
msgid "No data (%s)"
msgstr "No data (%s)"

#: ../src/lib/PollManager.py:300
#, python-format
msgid "Unable to find the category (%s: %s)"
msgstr "Unable to find the category (%s: %s)"

#: ../src/lib/PollManager.py:305
#, python-format
msgid "Updating category resulted in abnormal status '%s' (code %d)"
msgstr "Updating category resulted in abnormal status '%s' (code %d)"

#: ../src/lib/PreferencesDialog.py:91
msgid "_Category"
msgstr "_Category"

#: ../src/lib/PreferencesDialog.py:201
msgid "Category name"
msgstr "Category name"

#: ../src/lib/PreferencesDialog.py:291
msgid "_Member"
msgstr "_Member"

#: ../src/lib/PreferencesDialog.py:297
msgid "_Feed"
msgstr "_Feed"

#: ../src/lib/PreferencesDialog.py:546
msgid "Using desktop setting"
msgstr "Using desktop setting"

#: ../src/lib/subscribe.py:55
msgid "Feed Location must not be empty"
msgstr "Feed Location must not be empty"

#: ../src/lib/SummaryParser.py:177
msgid "No title"
msgstr "No title"

#: ../src/lib/Tray.py:77
#, python-format
msgid "%d new items"
msgstr "%d new items"

#. this is here just to make xgettext happy: it should be defined in
#. only one place, and a good one would be MainWindow.py module level.
#. however, we can't access _ there.
#. The format: %A is the full weekday name
#. %B is the full month name
#. %e is the day of the month as a decimal number,
#. without leading zero
#. This should be translated to be suitable for the locale in
#. question, feel free to alter the order and the parameters (they
#. are strftime(3) parameters, the whole string is passed to the
#. function, Straw does no additional interpretation) if you feel
#. it's necessary in the translation file.
#: ../src/lib/utils.py:110
msgid "%A %B %e %H:%M"
msgstr "%A %B %e %H:%M"

#: ../src/lib/utils.py:180
msgid "An error occurred while trying to open link"
msgstr "An error occurred while trying to open link"

#: ../src/lib/utils.py:181
#, python-format
msgid ""
"There was a problem opening '%s'\n"
"\n"
"Error thrown is '%s'"
msgstr ""
"There was a problem opening '%s'\n"
"\n"
"Error thrown is '%s'"

#: ../straw.desktop.in.h:1
msgid "Aggregates news and blog feeds"
msgstr "Aggregates news and blog feeds"

#: ../straw.desktop.in.h:2
msgid "Feed Reader"
msgstr "Feed Reader"

#: ../straw.desktop.in.h:3
msgid "Straw Feed Reader"
msgstr "Straw Feed Reader"

#~ msgid "  "
#~ msgstr "  "
#~ msgid "<b><big>Subscribe to Feed</big></b>"
#~ msgstr "<b><big>Subscribe to Feed</big></b>"
#~ msgid "<b>Authorization</b>"
#~ msgstr "<b>Authorisation</b>"
#~ msgid "<b>Categories</b>"
#~ msgstr "<b>Categories</b>"
#~ msgid "<b>Polling and Article Storage</b>"
#~ msgstr "<b>Polling and Article Storage</b>"
#~ msgid "<b>Refresh times</b>"
#~ msgstr "<b>Refresh times</b>"
#~ msgid "<b>Settings</b>"
#~ msgstr "<b>Settings</b>"
#~ msgid "<b>Source Information</b>"
#~ msgstr "<b>Source Information</b>"
#~ msgid "<big><b>Authorization Required</b></big>"
#~ msgstr "<big><b>Authorisation Required</b></big>"
#~ msgid "Auth"
#~ msgstr "Auth"
#~ msgid "Connecting to ..."
#~ msgstr "Connecting to ..."
#~ msgid "Feed Properties"
#~ msgstr "Feed Properties"
#~ msgid "Feed URL:"
#~ msgstr "Feed URL:"
#~ msgid "Main"
#~ msgstr "Main"
#~ msgid "Multiple Feed"
#~ msgstr "Multiple Feed"
#~ msgid "Progress"
#~ msgstr "Progress"
#~ msgid "Refresh"
#~ msgstr "Refresh"
#~ msgid "Scroll Unread"
#~ msgstr "Scroll Unread"
#~ msgid "Select the categories to which this feed should belong to."
#~ msgstr "Select the categories to which this feed should belong to."
#~ msgid "Select the feeds you want to subscribe to below:"
#~ msgstr "Select the feeds you want to subscribe to below:"
#~ msgid "Subscribe"
#~ msgstr "Subscribe"
#~ msgid "The site you're accessing requires authorization."
#~ msgstr "The site you're accessing requires authorisation."
#~ msgid "Use this only if you need authorization to read this feed."
#~ msgstr "Use this only if you need authorisation to read this feed."
#~ msgid "_Authorization"
#~ msgstr "_Authorisation"
#~ msgid "_Continue"
#~ msgstr "_Continue"
#~ msgid "_Done"
#~ msgstr "_Done"
#~ msgid "_OK"
#~ msgstr "_OK"
#~ msgid "_Settings"
#~ msgstr "_Settings"
#~ msgid "_Title:"
#~ msgstr "_Title:"
#~ msgid "Feed URL Not Provided"
#~ msgstr "Feed URL Not Provided"
#~ msgid "Please provide the URL of the feed you are trying to subscribe to."
#~ msgstr "Please provide the URL of the feed you are trying to subscribe to."
#~ msgid "Bad Feed URL"
#~ msgstr "Bad Feed URL"
#~ msgid "Straw wasn't able to parse '%s'"
#~ msgstr "Straw wasn't able to parse '%s'"
#~ msgid "Unsupported Scheme"
#~ msgstr "Unsupported Scheme"
#~ msgid "Subscribing to '%s://' is not supported"
#~ msgstr "Subscribing to '%s://' is not supported"
#~ msgid "Title"
#~ msgstr "Title"
#~ msgid "Searching %s"
#~ msgstr "Searching %s"
#~ msgid "Searching %s. This may take a while..."
#~ msgstr "Searching %s. This may take a while..."
#~ msgid "Unexpected Error Occurred"
#~ msgstr "Unexpected Error Occurred"
#~ msgid "Unable to Find Feeds"
#~ msgstr "Unable to Find Feeds"
#~ msgid "Straw was unable to find feeds in %s"
#~ msgstr "Straw was unable to find feeds in %s"
#~ msgid "Processing %d of %d feeds"
#~ msgstr "Processing %d of %d feeds"
#~ msgid "Error While Subscribing"
#~ msgstr "Error While Subscribing"
#~ msgid "No Data"
#~ msgstr "No Data"
#~ msgid "Host name lookup failed"
#~ msgstr "Host name lookup failed"
#~ msgid "Maximum download time exceeded"
#~ msgstr "Maximum download time exceeded"
#~ msgid "Maximum download file size exceeded"
#~ msgstr "Maximum download file size exceeded"
#~ msgid "Feed is empty."
#~ msgstr "Feed is empty."
#~ msgid "<b>General</b>"
#~ msgstr "<b>General</b>"
#~ msgid "*"
#~ msgstr "*"
#~ msgid "<b>Sort Category Contents</b>"
#~ msgstr "<b>Sort Category Contents</b>"
#~ msgid "Default _refresh frequency:"
#~ msgstr "Default _refresh frequency:"
#~ msgid "Find Items"
#~ msgstr "Find Items"
#~ msgid "Username:"
#~ msgstr "Username:"
#~ msgid "_Articles:"
#~ msgstr "_Articles:"
#~ msgid "_Default number of articles stored per feed:"
#~ msgstr "_Default number of articles stored per feed:"
#~ msgid "_Scroll or Next Unread"
#~ msgstr "_Scroll or Next Unread"
#~ msgid "_Subscribe"
#~ msgstr "_Subscribe"
#~ msgid "articles"
#~ msgstr "articles"
#~ msgid "bottom"
#~ msgstr "bottom"
#~ msgid "minutes"
#~ msgstr "minutes"
#~ msgid "top"
#~ msgstr "top"
#~ msgid "Orientation"
#~ msgstr "Orientation"
#~ msgid "The orientation of the tray."
#~ msgstr "The orientation of the tray."
#~ msgid ""
#~ "\n"
#~ "                Straw will not behave as excepted, you should probably "
#~ "quit now.\n"
#~ "                The exception has been saved to the file %s. Please see\n"
#~ "                the Straw README for further instructions.\n"
#~ "                "
#~ msgstr ""
#~ "\n"
#~ "                Straw will not behave as expected, you should probably "
#~ "quit now.\n"
#~ "                The exception has been saved to the file %s. Please see\n"
#~ "                the Straw README for further instructions.\n"
#~ "                "
#~ msgid "No data yet, need to poll first."
#~ msgstr "No data yet, need to poll first."
#~ msgid "Title:"
#~ msgstr "Title:"
#~ msgid "Date:"
#~ msgstr "Date:"
#~ msgid "Feed:"
#~ msgstr "Feed:"
#~ msgid "/Mark as _Unread"
#~ msgstr "/Mark as _Unread"
#~ msgid "Find text in articles"
#~ msgstr "Find text in articles"
#~ msgid "/sep"
#~ msgstr "/sep"
#~ msgid "/_Remove"
#~ msgstr "/_Remove"
#~ msgid "/P_roperties"
#~ msgstr "/P_roperties"
#~ msgid "Straw is a desktop news aggregator for GNOME"
#~ msgstr "Straw is a desktop news aggregator for GNOME"
#~ msgid "Connecting to '%s'"
#~ msgstr "Connecting to '%s'"
#~ msgid "Enter username and password for '%s'"
#~ msgstr "Enter username and password for '%s'"
#~ msgid "Getting feed info ...."
#~ msgstr "Getting feed info ...."
#~ msgid "Forbidden"
#~ msgstr "Forbidden"
#~ msgid "Export Error"
#~ msgstr "Export Error"
#~ msgid "Unable to import subscriptions"
#~ msgstr "Unable to import subscriptions"
#~ msgid "Error occurred while reading file: %s"
#~ msgstr "Error occurred while reading file: %s"
#~ msgid "Straw Desktop News Aggregator"
#~ msgstr "Straw Desktop News Aggregator"
#~ msgid "Always display _new items at the "
#~ msgstr "Always display _new items at the "
#~ msgid "Authentication Required"
#~ msgstr "Authentication Required"
#~ msgid "Authentication wasn't successful. Please try again."
#~ msgstr "Authentication wasn't successful. Please try again."
#~ msgid "Categories"
#~ msgstr "Categories"
#~ msgid ""
#~ "Enter the location of the site that you want to subscribe to in the text "
#~ "box \n"
#~ "below. Press the <b>Forward</b> button if you are done"
#~ msgstr ""
#~ "Enter the location of the site that you want to subscribe to in the text "
#~ "box \n"
#~ "below. Press the <b>Forward</b> button if you are done"
#~ msgid "Found Feeds"
#~ msgstr "Found Feeds"
#~ msgid "News Location:"
#~ msgstr "News Location:"
#~ msgid ""
#~ "Select a category on the left to display the feeds in that category on "
#~ "the right."
#~ msgstr ""
#~ "Select a category on the left to display the feeds in that category on "
#~ "the right."
#~ msgid ""
#~ "This druid will guide you through the subscription process. Press the "
#~ "Forward button to continue."
#~ msgstr ""
#~ "This druid will guide you through the subscription process. Press the "
#~ "Forward button to continue."
#~ msgid "_Add"
#~ msgstr "_Add"
#~ msgid "_Display"
#~ msgstr "_Display"
#~ msgid "_Get feed items every"
#~ msgstr "_Get feed items every"
#~ msgid "items at once"
#~ msgstr "items at once"
#~ msgid "Warning: Not All Feeds Have Been Added"
#~ msgstr "Warning: Not All Feeds Have Been Added"
#~ msgid ""
#~ "Some feeds in the file %s were not added because they are not RSS feeds."
#~ msgstr ""
#~ "Some feeds in the file %s were not added because they are not RSS feeds."
#~ msgid ""
#~ "<b>An error occurred while reading this feed:</b>\n"
#~ "%s"
#~ msgstr ""
#~ "<b>An error occurred while reading this feed:</b>\n"
#~ "%s"
#~ msgid "Page"
#~ msgstr "Page"
#~ msgid "Refresh this category"
#~ msgstr "Refresh this category"
#~ msgid "Polling feeds..."
#~ msgstr "Polling feeds..."
#~ msgid "Poll Error"
#~ msgstr "Poll Error"
#~ msgid "The feed returned no data."
#~ msgstr "The feed returned no data."
#~ msgid "Unknown exception when parsing RSS: %s"
#~ msgstr "Unknown exception when parsing RSS: %s"
#~ msgid "Polling of feed %s failed"
#~ msgstr "Polling of feed %s failed"
#~ msgid "Feed Found"
#~ msgstr "Feed Found"
#~ msgid ""
#~ "Press the Apply button if you want to subscribe to the feed below.\n"
#~ "\n"
#~ msgstr ""
#~ "Press the Apply button if you want to subscribe to the feed below.\n"
#~ "\n"
#~ msgid "Title: %s \n"
#~ msgstr "Title: %s \n"
#~ msgid "Description: %s \n"
#~ msgstr "Description: %s \n"
#~ msgid ""
#~ "Press the Apply button if you want to subscribe to the following feeds:\n"
#~ "\n"
#~ msgstr ""
#~ "Press the Apply button if you want to subscribe to the following feeds:\n"
#~ "\n"
#~ msgid "\t%s\n"
#~ msgstr "\t%s\n"
#~ msgid ""
#~ "No feed was found in %s. Please make sure you typed the correct location."
#~ msgstr ""
#~ "No feed was found in %s. Please make sure you typed the correct location."
#~ msgid "Fatal Error"
#~ msgstr "Fatal Error"
#~ msgid ""
#~ "A fatal error occurred while processing this operation. Please report it "
#~ "to the maintainer. Error: %s"
#~ msgstr ""
#~ "A fatal error occurred while processing this operation. Please report it "
#~ "to the maintainer. Error: %s"
#~ msgid "Exception occurred %s in feed %s"
#~ msgstr "Exception occurred %s in feed %s"
#~ msgid "Error parsing item %s: %s"
#~ msgstr "Error parsing item %s: %s"

