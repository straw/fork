from pysqlite2 import dbapi2 as sqlite
import threading

DATABASE_FILE_NAME = "data.db"

class SQLiteStorage:
    """
    Provides an entry point to the database.

    """

    def __init__(self, db_path):
        self._connections = {}
        self._txs = {}
        self._db_path = db_path
        self._init_db()

    def _get_sql(self):
        f = open('data/sql/create_01.sql', 'r')
        sql = f.read()
        f.close()
        return sql.split("--")
    
    def _init_db(self):
        do_init = False

        try:
            # Check if we need init.
            # TODO: use smarter strategy maybe?
            result = self.query("SELECT COUNT(*) FROM feeds")
        except Exception:
            # DB seems not initialized.
            do_init = True

        if not do_init:
            return

        c = self._connect()
        
        #c.execute("PRAGMA cache_size = 20000")

        try:
            sql = self._get_sql()
            for statement in sql:
                c.execute(statement)
            self._tx_commit()
        except Exception, e:
            print "DB init failed -- %s" % e

    def _connect(self):
        key = threading.currentThread()

        if not self._connections.has_key(key):
            self._connections[key] = sqlite.connect(self._db_path)
            self._connections[key].row_factory = sqlite.Row

        return self._connections[key]
    
    def _tx_begin(self):
        key = threading.currentThread()

        if not self._txs.has_key(key):
            self._txs[key] = True

    def _tx_commit(self):
        self._connect().commit()

        key = threading.currentThread()
        self._txs[key] = False
    
    def _in_tx(self):
        key = threading.currentThread()
        return self._txs.has_key(key) and self._txs[key]

    def query(self, query, params = None):
        if params == None:
            params = ()
        cursor = self._connect()
        list = []
        res = cursor.execute(query, params)
        return res.fetchall()

    def insert(self, table, data):
        """
        Inserts some data into the database.

        @param table: a name of the table to insert to
        @param data: a dictionary where keys are field names and values
            are field values in the given table

        """
        
        if len(data) == 0:
            return None

        cursor = self._connect().cursor()
        query = "INSERT INTO %s (%s) VALUES (?%s)" % (table, ", ".join(data.keys()),
                                                      ", ?" * (len(data.keys()) - 1))
        #print query
        #print data
        cursor.execute(query, data.values())

        #print self._in_tx()
        if not self._in_tx(): 
            self._tx_commit()

        return cursor.lastrowid

    def update(self, table, id, data):
        """
        Updates single row identified by a primary key in the table.

        @param table: a name of the table to do the update in
        @param id: primary key of the item to update
        @param data: a dictionary where keys are field names and values
            are field values in the given table

        """

        params = data.values()
        cursor = self._connect().cursor()
        assignments = ", ".join([("%s = ?" % field_name) for field_name in data.keys()])
        query = "UPDATE %s SET %s WHERE id = ?" % (table, assignments)
        #print query
        params.append(id)
        cursor.execute(query, params)

        if not self._in_tx(): 
            self._tx_commit()

        return cursor.lastrowid
