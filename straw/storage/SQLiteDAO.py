from straw.model import Category, Feed
import SQLiteStorage as Storage

class DAO(object):
    def __init__(self, storage):
        self.storage = storage
    
    def _get_indexed(self, entities, index_field):
        return dict([(getattr(entity, index_field), entity) for entity in entities])

    def _get_grouped(self, entities, field):
        groups = {}
        previous = None

        for entity in entities:
            current = getattr(entity, field)

            if previous != current:
                # we have new group, let's make a dict entry for it
                groups[current] = []
                previous = current

            if current != None:
                groups[current].append(entity)

        return groups

    def _is_inherited(self, clazz):
        return len(clazz.__bases__) == 1 and hasattr(clazz, "inheritance") and clazz.inheritance

    def _get_persistent_fields(self, clazz):
        fields = list(clazz.persistent_properties)

        if self._is_inherited(clazz):
            fields.extend(clazz.__bases__[0].persistent_properties)

        return fields
    
    def tx_begin(self):
        self.storage._tx_begin()
    
    def tx_commit(self):
        self.storage._tx_commit()

    def save(self, entity):
        do_insert = entity.id == None

        if self._is_inherited(entity.__class__):
            base_class = entity.__class__.__bases__[0]
            base_data = dict([(property, getattr(entity, property)) for property in base_class.persistent_properties if property != base_class.primary_key])

            if do_insert:
                id = self.storage.insert(base_class.persistent_table, base_data)
                entity.id = id
            else:
                self.storage.update(base_class.persistent_table, entity.id, base_data)

        data = dict([(property, getattr(entity, property)) for property in entity.__class__.persistent_properties if property != entity.__class__.primary_key])

        if do_insert:
            id = self.storage.insert(entity.__class__.persistent_table, data)
            
            if entity.id == None:
                entity.id = id
        else:
            self.storage.update(entity.__class__.persistent_table, entity.id, data)

        #print "save = %s" % str(time.time() - s)
        return entity.id
    
    def get_one(self, clazz, id):
        #result = self.storage.query("SELECT * FROM %s WHERE id = ?" % clazz.persistent_table, params = (id,))
        result = self.get(clazz, " WHERE id = %s" % id)
        return result[0]

    def get(self, clazz, sql = "", params = []):
        result = self.storage.query("SELECT * FROM %s%s" % (clazz.persistent_table, sql), params)
        entities = []
        
        for row in result:
            entity = clazz()

            i = 0
            for field in clazz.persistent_properties:
                setattr(entity, field, row[i])
                i += 1

            entities.append(entity)

        return entities
    
    def get_with_params(self, clazz, params):
        #self.storage.query(query, params = None)
        where_clause = ""
        for key in params.keys():
            where_clause
        self.storage.query("SELECT * FROM %s WHERE %s" % (clazz.persistent_table, where_clause))

    def get_indexed(self, clazz, index_field, sql = ""):
        result = self.storage.query("SELECT * FROM %s%s" % (clazz.persistent_table, sql))
        entities = {}

        for row in result:
            entity = clazz()

            i = 0
            for field in clazz.persistent_properties:
                setattr(entity, field, row[i])
                i += 1

            entities[getattr(entity, index_field)] = entity

        return entities

    def get_nodes(self, sql = ""):
        #result = self.storage.query("SELECT f.*, feed_id, unread_count FROM feeds f LEFT JOIN (SELECT COUNT(*) AS unread_count, feed_id FROM items i WHERE i.is_read = 0 GROUP BY i.feed_id) ON feed_id = f.id ORDER BY category_id")
        result = self.storage.query("SELECT *, unread_count FROM nodes n LEFT JOIN feeds f ON (f.id = n.id) LEFT JOIN categories c ON (c.id = n.id) LEFT JOIN (SELECT COUNT(*) AS unread_count, feed_id FROM items i WHERE i.is_read = 0 GROUP BY i.feed_id) ON feed_id = f.id ORDER BY n.parent_id, n.norder")
        entities = []

        for row in result:
            if row["type"] == "C":
                clazz = Category
            elif row["type"] == "F":
                clazz = Feed

            fields = self._get_persistent_fields(clazz)
            entity = clazz()
            entity.unread_count = 0

            for field in fields:
                value = row[field]
                #if row[field] == None:
                #    value = 0

                setattr(entity, field, value)

            if row["unread_count"] != None:
                setattr(entity, "unread_count", row["unread_count"])

            entities.append(entity)

        return self._get_indexed(entities, "id"), self._get_grouped(entities, "parent_id")
