""" FeedListView.py

Module for displaying the feeds in the Feeds TreeView.
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from model import Feed, Item, Category
import Config
import FeedManager
import MVP
import PollManager
import gobject
import gtk
import helpers
import os, copy, locale, logging
import pango
import straw

class Column:
    pixbuf, name, foreground, unread, object = range(5)

_tmp_widget = gtk.Label()

class TreeViewNode(object):
    def __init__(self, node, store):
        self.node = node
        self.store = store

        self.setup_node()

    def setup_node(self):
        self.node.connect("notify", self.obj_changed)

    def obj_changed(self, obj, property):
        if property.name == "unread-count":
            self.store.set(self.iter, 3, self.node.unread_count)
        elif property.name == "status":
            if (self.node.status & straw.FS_UPDATING) > 0:
                title = self.store.get_value(self.iter, 1)
                self.store.set(self.iter, 1, "<i>" + title + "</i>")
            else:
                title = self.node.title
                self.store.set(self.iter, 1, title)

    @property
    def title(self):
        ''' The title of the node be it a category or a feed '''
        if self.node.type == "C":
            return self.node.name
        elif self.node.type == "F":
            return self.node.title

    @property
    def unread_count(self):
        ''' The title of the node be it a category or a feed '''
        return self.node.unread_count

    @property
    def pixbuf(self):
        ''' gets the pixbuf to display according to the status of the feed '''
        global _tmp_widget

        if isinstance(self.node, Feed):
            return _tmp_widget.render_icon(gtk.STOCK_FILE, gtk.ICON_SIZE_MENU)
        else:
            return _tmp_widget.render_icon(gtk.STOCK_DIRECTORY, gtk.ICON_SIZE_MENU)

    @property
    def path_list(self):
        path = []

        node = copy.copy(self.node)

        while node != None:
            path.append(str(node.norder))
            node = copy.copy(node.parent)

        path.pop() # We don't need path to "root" category here since it's not in the tree view.
        path.reverse()

        return path
    
    @property
    def path(self):
        path = self.path_list

        if len(path) == 0:
            return None
        else:
            return ":".join(path)
    
    @property
    def iter(self):
        path = self.path

        if path == None:
            return None
        else:
            return self.store.get_iter_from_string(path)

    @property
    def parent_path(self):
        path = self.path_list
        path.pop()

        if len(path) == 0:
            return None
        else:
            return ":".join(path)

    @property
    def parent_iter(self):
        path = self.parent_path

        if path == None:
            return None
        else:
            return self.store.get_iter_from_string(path)

class FeedListModel:
    ''' The model for the feed list view '''

    def __init__(self):
        self.refresh_tree()
        self._init_signals()

    def refresh_tree(self):
        self.categories, self.appmodel = FeedManager.get_model()

        self._prepare_store()
        self._prepare_model()
        
        #print self.tv_nodes[1]

        self._populate_tree(1, None, [])

    def _init_signals(self):
        FeedManager._get_instance().connect("feed-added", self.node_added_cb)
        FeedManager._get_instance().connect("category-added", self.node_added_cb)

    def _prepare_model(self):
        self.tv_nodes = {}

        for parent_id in self.appmodel.keys():
            self.tv_nodes[parent_id] = [TreeViewNode(node, self.store) for node in self.appmodel[parent_id]]

    def _prepare_store(self):
        self.store = gtk.TreeStore(gtk.gdk.Pixbuf, str, str, str, gobject.TYPE_PYOBJECT)

    def _populate_tree(self, parent_id, parent_iter, done):
        if not self.tv_nodes.has_key(parent_id):
            return

        for tv_node in self.tv_nodes[parent_id]:
            node = tv_node.node

            if node.type == "F":
                tv_node.treeiter = self._create_row(tv_node)
                tv_node.store = self.store
            elif node.type == "C":
                current_parent = self._create_row(tv_node)
                tv_node.treeiter = current_parent
                tv_node.store = self.store

                if self.tv_nodes.has_key(node.id):
                    self._populate_tree(node.id, current_parent, done)

    def _create_row(self, node):
        return self.store.append(node.parent_iter, [node.pixbuf,
                                              node.title,
                                              'black',
                                              node.unread_count,
                                              node])

    def _lookup_parent(self, _tv_node):
        for tv_node in self.tv_nodes[_tv_node.node.parent_id]:
            if tv_node.node.type == "C" and _tv_node.node.parent_id == tv_node.node.id:
                    return tv_node

    def add_node(self, tv_node):
        if not self.tv_nodes.has_key(tv_node.node.parent_id):
            self.tv_nodes[tv_node.node.parent_id] = []

        self.tv_nodes[tv_node.node.parent_id].append(tv_node)

    def node_added_cb(self, src, node):
        tv_node = TreeViewNode(node, self.store)
        self.add_node(tv_node)

        self._create_row(tv_node)

        """if parent_node != None and hasattr(parent_node, "treeiter"):
            parent_iter = parent_node.treeiter
        tv_node.treeiter = self._create_row(tv_node, parent_iter)
        tv_node.store = self.store"""

    @property
    def model(self):
        return self.store

    def search(self, rows, func, data):
        if not rows: return None
        for row in rows:
            if func(row, data):
                return row
            result = self.search(row.iterchildren(), func, data)
            if result: return result
        return None

class FeedsView(MVP.WidgetView):
    def _initialize(self):
        self._widget.set_search_column(Column.name)

        # pixbuf column
        column = gtk.TreeViewColumn()
        unread_renderer = gtk.CellRendererText()
        column.pack_start(unread_renderer, False)
        column.set_attributes(unread_renderer,
                              text=Column.unread)

        status_renderer = gtk.CellRendererPixbuf()
        column.pack_start(status_renderer, False)
        column.set_attributes(status_renderer,
                              pixbuf=Column.pixbuf)

        # feed title renderer
        title_renderer = gtk.CellRendererText()
        column.pack_start(title_renderer, False)
        column.set_attributes(title_renderer,
                              foreground=Column.foreground,
                              markup=Column.name) #, weight=Column.BOLD)

        self._widget.append_column(column)

        selection = self._widget.get_selection()
        selection.set_mode(gtk.SELECTION_SINGLE)

        self._widget.connect("button_press_event", self._on_button_press_event)
        self._widget.connect("popup-menu", self._on_popup_menu)

        uifactory = helpers.UIFactory('FeedListActions')
        action = uifactory.get_action('/feedlist_popup/refresh')
        action.connect('activate', self.on_menu_poll_selected_activate)
        action = uifactory.get_action('/feedlist_popup/mark_as_read')
        action.connect('activate', self.on_menu_mark_all_as_read_activate)
        action = uifactory.get_action('/feedlist_popup/stop_refresh')
        action.connect('activate', self.on_menu_stop_poll_selected_activate)
        action = uifactory.get_action('/feedlist_popup/remove')
        action.connect('activate', self.on_remove_selected_feed)
        action = uifactory.get_action('/feedlist_popup/properties')
        action.connect('activate', self.on_display_properties_feed)
        self.popup = uifactory.get_popup('/feedlist_popup')

        treeview = self._widget

        treeview.enable_model_drag_source(gtk.gdk.BUTTON1_MASK,
                 [("example", 0, 0)], gtk.gdk.ACTION_COPY)
        treeview.enable_model_drag_dest([("example", 0, 0)],
                 gtk.gdk.ACTION_COPY)
        treeview.connect("drag_data_received", self.on_dragdata_received_cb)

    def on_dragdata_received_cb(self, treeview, drag_context, x, y, selection, info, eventtime):
        model, iter_to_copy = treeview.get_selection().get_selected()
        #print locals()
        temp = treeview.get_dest_row_at_pos(x, y)

        #temp = treeview.get_drag_dest_row()

        if temp != None:
            path, pos = temp
        else:
            path, pos = (len(model)-1,), gtk.TREE_VIEW_DROP_AFTER

        target_iter = model.get_iter(path)
        path_of_target_iter = model.get_path(target_iter)

        if self.check_row_path(model, iter_to_copy, target_iter):
            path = model.get_path(iter_to_copy)
            #model.insert_before(None, None, model[path])
            #print model[path][Column.object].obj.title
            #print "previous = %s" % str(path)
            #print "target = %s" % str(path_of_target_iter)
            #print path_of_target_iter[len(path_of_target_iter) - 1]

            from_path = path
            to_path = list(path_of_target_iter)

            if (pos == gtk.TREE_VIEW_DROP_INTO_OR_BEFORE) or (pos == gtk.TREE_VIEW_DROP_INTO_OR_AFTER):
                pass#new_iter = model.prepend(target_iter, model[path])
            elif pos == gtk.TREE_VIEW_DROP_BEFORE:
                pass
            elif pos == gtk.TREE_VIEW_DROP_AFTER:
                to_path = list(path_of_target_iter[0:(len(path_of_target_iter) - 2)])
                to_path.append(path_of_target_iter[len(path_of_target_iter) - 1] + 1)

            print "%s -> %s" % (str(from_path), str(to_path))

            new_order = to_path.pop()
            
            FeedManager.move_node(model[from_path][Column.object].node,
                                  model[":".join(map(str, to_path))][Column.object].node, new_order)

            self.iter_copy(model, iter_to_copy, target_iter, pos)
            drag_context.finish(True, True, eventtime)
            #model.remove(iter_to_copy)
            #treeview.expand_all()
        else:
            drag_context.finish(False, False, eventtime)

    def check_row_path(self, model, iter_to_copy, target_iter):
        path_of_iter_to_copy = model.get_path(iter_to_copy)
        path_of_target_iter = model.get_path(target_iter)
        if path_of_target_iter[0:len(path_of_iter_to_copy)] == path_of_iter_to_copy:
            return False
        else:
            return True 

    def iter_copy(self, model, iter_to_copy, target_iter, pos):
        path = model.get_path(iter_to_copy)

        if (pos == gtk.TREE_VIEW_DROP_INTO_OR_BEFORE) or (pos == gtk.TREE_VIEW_DROP_INTO_OR_AFTER):
            new_iter = model.prepend(target_iter, model[path])
        elif pos == gtk.TREE_VIEW_DROP_BEFORE:
            new_iter = model.insert_before(None, target_iter, model[path])
        elif pos == gtk.TREE_VIEW_DROP_AFTER:
            new_iter = model.insert_after(None, target_iter, model[path])

        n = model.iter_n_children(iter_to_copy)
        for i in range(n): 
            next_iter_to_copy = model.iter_nth_child(iter_to_copy, n - i - 1)
            self.iter_copy(model, next_iter_to_copy, new_iter, gtk.TREE_VIEW_DROP_INTO_OR_BEFORE)

    def _model_set(self):
        self._widget.set_model(self._model.model)

    def add_selection_changed_listener(self, listener):
        selection = self._widget.get_selection()
        selection.connect('changed', listener.feedlist_selection_changed, Column.object)

    def _on_popup_menu(self, treeview, *args):
        self.popup.popup(None, None, None, 0, 0)

    def _on_button_press_event(self, treeview, event):
        retval = 0
        if event.button == 3:
            x = int(event.x)
            y = int(event.y)
            time = gtk.get_current_event_time()
            path = treeview.get_path_at_pos(x, y)
            if path is None:
                return 1
            path, col, cellx, celly = path
            selection = treeview.get_selection()
            selection.unselect_all()
            selection.select_path(path)
            treeview.grab_focus()
            self.popup.popup(None, None, None, event.button, time)
            retval = 1
        return retval

    def foreach_selected(self, func):
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        try:
            for treeiter in iters:
                object = model.get_value(treeiter, Column.object)
                func(object, model, treeiter)
        except TypeError, te:
            ## XXX maybe object is a category
            logging.exception(te)
        return

    def on_menu_poll_selected_activate(self, *args):
        config = Config.get_instance()
        poll = True
        if config.offline: #XXX
            config.offline = not config.offline
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        nodes = [model.get_value(treeiter,Column.object) for treeiter in iters]
        fds = []
        for n in nodes:
            try:
                fds.append(n.feed)
            except TypeError:
                fds += n.category.feeds
        if fds:
            PollManager.get_instance().poll(fds)
        return

    def on_menu_stop_poll_selected_activate(self, *args):
        self.foreach_selected(lambda o,*args: o.feed.router.stop_polling())

    def on_menu_mark_all_as_read_activate(self, *args):
        self.foreach_selected(lambda o,*args: o.feed.mark_items_as_read())

    def on_remove_selected_feed(self, *args):
        def remove(*args):
            (object, model, treeiter) = args
            model.remove(treeiter)
            feedlist = feeds.get_feedlist_instance()
            idx = feedlist.index(object.feed)
            del feedlist[idx]
        self.foreach_selected(remove)
        return

    def on_display_properties_feed(self, *args):
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        path = pathlist.pop()
        node = self.model.model[path][Column.object]
        self._presenter.show_feed_information(node)
        return

    def select_first_feed(self):
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        treeiter = model.get_iter_first()
        if not treeiter or not model.iter_is_valid(treeiter):
            return False
        self.set_cursor(treeiter)
        return True

    def select_next_feed(self, with_unread=False):
        ''' Scrolls to the next feed in the feed list

        If there is no selection, selects the first feed. If multiple feeds
        are selected, selects the feed after the last selected feed.

        If unread is True, selects the next unread with unread items.

        If the selection next-to-be is a category, go to the iter its first
        child. If current selection is a child, then go to (parent + 1),
        provided that (parent + 1) is not a category.
        '''
        has_unread = False
        def next(model, current):
            treeiter = model.iter_next(current)
            if not treeiter: return False
            if model.iter_depth(current): next(model, model.iter_parent(current))
            path = model.get_path(treeiter)
            if with_unread and model[path][Column.unread] < 1:
                next(model, current)
            self.set_cursor(treeiter)
            return True
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        try:
            current = iters.pop()
            if model.iter_has_child(current):
                iterchild = model.iter_children(current)
                # make the row visible
                path = model.get_path(iterchild)
                for i in range(len(path)):
                    self._widget.expand_row(path[:i+1], False)
                # select his first born child
                if with_unread and model[path][Column.unread] > 0:
                    self.set_cursor(iterchild)
                    has_unread = True
                else:
                    has_unread = next(model, current)
            has_unread = next(model,current)
        except IndexError:
            self.set_cursor(model.get_iter_first())
            has_unread = True
        return has_unread

    def select_previous_feed(self):
        ''' Scrolls to the previous feed in the feed list.

        If there is no selection, selects the first feed. If there's multiple
        selection, selects the feed before the first selected feed.

        If the previous selection is a category, select the last node in that
        category. If the current selection is a child, then go to (parent -
        1). If parent is the first feed, wrap and select the last feed or
        category in the list.
        '''
        def previous(model, current):
            path = model.get_path(current)
            treerow = model[path[-1]-1]
            self.set_cursor(treerow.iter)
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        try:
            current_first = iters.pop(0)
            if model.iter_has_child(current_first):
                children = model.iter_n_children(current_first)
                treeiter = model.iter_nth_child(children - 1)
                self.set_cursor(treeiter)
                return
            previous(model, current_first)
        except IndexError:
            self.set_cursor(model.get_iter_first())
        return

    def set_cursor(self, treeiter, col_id=None, edit=False):
        if not treeiter:
            return
        column = None
        path = self._model.model.get_path(treeiter)
        if col_id:
            column = self._widget.get_column(col_id)
        self._widget.set_cursor(path, column, edit)
        self._widget.scroll_to_cell(path, column)
        self._widget.grab_focus()
        return

class FeedsPresenter(MVP.BasicPresenter):
    def _initialize(self):
        self.model = FeedListModel()
        self._init_signals()

    def _init_signals(self):
        
        pass
        #flist.signal_connect(Event.ItemReadSignal,
        #                     self._feed_item_read)
        #flist.signal_connect(Event.AllItemsReadSignal,
        #                     self._feed_all_items_read)
        #flist.signal_connect(Event.FeedsChangedSignal,
        #                     self._feeds_changed)
        #flist.signal_connect(Event.FeedDetailChangedSignal,
        #                     self._feed_detail_changed)
        #fclist = FeedCategoryList.get_instance()
        #fclist.signal_connect(Event.FeedCategorySortedSignal,
        #                      self._feeds_sorted_cb)
        #fclist.signal_connect(Event.FeedCategoryChangedSignal,
        #                      self._fcategory_changed_cb)

    def select_first_feed(self):
        return self.view.select_first_feed()

    def select_next_feed(self, with_unread=False):
        return self.view.select_next_feed(with_unread)

    def select_previous_feed(self):
        return self.view.select_previous_feed()

    def _sort_func(self, model, a, b):
        """
        Sorts the feeds lexically.

        From the gtk.TreeSortable.set_sort_func doc:

        The comparison callback should return -1 if the iter1 row should come before
        the iter2 row, 0 if the rows are equal, or 1 if the iter1 row should come
        after the iter2 row.
        """
        retval = 0
        fa = model.get_value(a, Column.OBJECT)
        fb = model.get_value(b, Column.OBJECT)

        if fa and fb:
            retval = locale.strcoll(fa.title, fb.title)
        elif fa is not None: retval = -1
        elif fb is not None: retval = 1
        return retval

    def show_feed_information(self, feed):
        straw.feed_properties_show(feed)
