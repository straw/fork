""" FeedUpdater.py

"""

__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from straw.JobManager import Job, TaskResult, TaskThread, TaskInfo, ThreadPoolJobHandler
import time
from threading import Lock

class FeedUpdateJobHandler(ThreadPoolJobHandler):
    job_id = "feed-update"

    def __init__(self, job):
        ThreadPoolJobHandler.__init__(self, job)

        self.pool_size = 3
        self.result_class = FeedUpdateTaskResult
        self.task_class = FeedUpdateTaskThread

    def _split(self):
        i = 0
        for a in self.job.data:
            i += 1
            ti = TaskInfo(i, { "feed" : a })
            self.task_queue.put(ti)

    def _prepare_result(self):
        list = []
        
        while not self.result_queue.empty():
            list.append(self.result_queue.get())

        return list

class FeedUpdateTaskResult(TaskResult):
    def __init__(self, task_info, result):
        self.task_info = task_info
        self.result = result

    def get(self):
        raise NotImplementedError

class FeedUpdateTaskThread(TaskThread):
    def __init__(self, handler, task_queue, result_queue):
        TaskThread.__init__(self, handler, task_queue, result_queue)

    def _process(self, task):
        import httplib2
        feed = task.data["feed"]

        import os
        from straw import Config
        CACHE_DIR = os.path.join(Config.straw_home(), 'cache')
        h = httplib2.Http(CACHE_DIR)
        #httplib2.debuglevel=4
        url = feed.location#feed.location
        resp, content = h.request(url, "GET")
        parsed = None

        print resp.fromcache

        import SummaryParser
        parsed = SummaryParser.parse(content, feed)

        #i = 0
        #for image in sum([item.images for item in parsed.items], []):
            #resp, content = h.request(image, "GET")
            #    i = i + 1
                #f = open("/home/ppawel/Desktop/test/%s" % i, "w")
                #f.write(content)
                #f.close()

        return parsed
    
    def _prepare_task_info(self, task_info):
        return task_info.data["feed"]

import straw.JobManager as JobManager

JobManager.register_handler(FeedUpdateJobHandler)

def update_urls(urls, observers):
    update = Job("feed-update")
    update.data = urls
    update.observers = observers
    JobManager.start_job(update, True)

def update_feeds(feeds, observers):
    update = Job("feed-update")
    update.data = feeds
    update.observers = observers
    JobManager.start_job(update)
