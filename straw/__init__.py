from defs import *

# convenience functions
from feedproperties import feed_properties_show
from preferences import preferences_show
from subscribe import subscribe_show

from MessageManager import post_status_message, get_status_manager, start_services

# feed status flags

FS_OK          = 1
FS_IDLE        = 2
FS_UPDATING    = 4
