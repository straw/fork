from gobject import GObject
import gobject
import straw

class Node(GObject):
    """
    Represents a base of single node in the model tree. This class is meant
    for subclassing only, Node instances should not be used.
    """

    persistent_properties = [ "id", "parent_id", "type", "norder" ]
    persistent_table = "nodes"
    primary_key = "id"

    __gproperties__ = {
        "norder": (int, "parent_id", "norder", 0, 999999, 0, gobject.PARAM_READWRITE),
        "parent_id": (int, "parent_id", "parent_id", 0, 999999, 0, gobject.PARAM_READWRITE),
        "unread-count": (int, "unread-count", "unread-count", 0, 999999, 0, gobject.PARAM_READWRITE)
    }

    __gsignals__ = {
        "unread-count-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT))
    }

    def __init__(self):
        GObject.__init__(self)
        self.id = None
        self.parent_id = None
        self.unread_count = 0
        self.norder = 0

    def do_get_property(self, property):
        if property.name == "norder":
            return self.norder
        elif property.name == "parent_id":
            return self.parent_id
        elif property.name == "unread-count":
            return self.unread_count

    def do_set_property(self, property, value):
        if property.name == "unread-count":
            if value == None:
                value = 0

            if self.unread_count != value:
                delta = value - self.unread_count
                old_value = self.unread_count
                self.unread_count = value
                self.emit("unread-count-changed", old_value, delta)

    def on_unread_count_changed(self, obj, old_value, delta):
        if isinstance(obj, Feed) and obj.parent_id == self.id:
            self.props.unread_count += delta
        elif isinstance(obj, Category):
            self.props.unread_count += delta

class Category(Node):
    """
    Represents a category - a node that can have child nodes.
    """

    persistent_properties = [ "id", "name" ]
    persistent_table = "categories"
    primary_key = None
    inheritance = True

    def __init__(self):
        GObject.__init__(self)
        Node.__gobject_init__(self)
        Node.__init__(self)

        self.id = None
        self.name = ""
        self.type = "C"

    def do_get_property(self, property):
        if property.name == "title":
            return self.title
        else:
            return Node.do_get_property(self, property)

    def do_set_property(self, property, value):
        if property.name == "title":
            self.title = value
        else:
            Node.do_set_property(self, property, value)

class Feed(Node):
    """
    Represents a feed - a node that can contain items. Feeds cannot have
    child nodes.
    """

    persistent_properties = [ "id", "title", "location", "link" ]
    persistent_table = "feeds"
    primary_key = None
    inheritance = True

    __gproperties__ = {
        "title":       (str, "title", "title", "", gobject.PARAM_READWRITE), 
        "location":    (str, "location", "location", "", gobject.PARAM_READWRITE), 
        "status": (int, "status", "status", 0, 9999, 0, gobject.PARAM_READWRITE),
    }

    def __init__(self):
        GObject.__init__(self)
        Node.__gobject_init__(self)
        Node.__init__(self)

        self.id = None
        self.type = "F"
        self.items = []

    def do_get_property(self, property):
        if property.name == "title":
            return self.title
        elif property.name == "location":
            return self.title
        elif property.name == "status":
            return self.status
        else:
            return Node.do_get_property(self, property)

    def do_set_property(self, property, value):
        if property.name == "title":
            self.title = value
        elif property.name == "location":
            self.location = value
        elif property.name == "status":
            self.status = value
        else:
            Node.do_set_property(self, property, value)

    def add_item(self, item):
        self.items.append(item)

    def on_is_read_changed(self, obj, is_read):
        if is_read:
            self.props.unread_count -= 1
        else:
            self.props.unread_count += 1

class Item(GObject):
    persistent_properties = [ "id", "title", "feed_id", "is_read", "link", "pub_date", "description" ]
    persistent_table = "items"
    primary_key = "id"
    
    __gproperties__ = {
        "title":       (str, "title", "title", "", gobject.PARAM_READWRITE), 
        "location":    (str, "location", "location", "", gobject.PARAM_READWRITE), 
        "is-read":    (gobject.TYPE_BOOLEAN, "is-read", "is-read", False, gobject.PARAM_READWRITE)
       }
    
    __gsignals__ = {
        "is-read-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,))
    }

    def __init__(self):
        GObject.__init__(self)
        self.id = None
        self.title = ""
        self.feed_id = None
        self.is_read = False
        
        self.link = "link"
        self.pub_date = None
        self.description = "description"
        self.source = None
        self.guid = "guid"
        self.guidislink = False
        
        self.publication_name = "publication_name"
        self.publication_volume = "publication_volume"
        self.publication_number = "publication_number"
        self.publication_section = "publication_section"
        self.publication_starting_page = "publication_starting_page"
        
        self.fm_license = "fm_license"
        self.fm_changes = "fm_changes"
        
        self.creator = "creator"

        self.contributors = []
        self.enclosures = []
        self.license_urls = []

    def do_get_property(self, property):
        if property.name == "title":
            return self.title
        elif property.name == "is-read":
            return self.is_read
        else:
            raise AttributeError, "unknown property %s" % property.name

    def do_set_property(self, property, value):
        if property.name == "title":
            self.title = value
        elif property.name == "is-read":
            old_value = bool(self.is_read)
            self.is_read = value
            if old_value != value:
                self.emit("is-read-changed", int(value))
        else:
            raise AttributeError, "unknown property %s" % property.name
