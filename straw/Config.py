""" Config.py

Module for Straw's configuration settings.
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """ GNU General Public License

This program is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

import cPickle, os, traceback
import urllib
from error import log, logtb, logparam
import pygtk
pygtk.require('2.0')
import gconf
import gobject

GCONF_STRAW_ROOT = "/apps/straw"

OPTION_LAST_POLL = "/general/last_poll"
OPTION_ITEMS_STORED = "/general/number_of_items_stored"
OPTION_ITEM_ORDER = "/general/item_order_newest"
OPTION_BROWSER_CMD = "/general/browser_cmd"
OPTION_WINDOW_SIZE_W = "/ui/window_width"
OPTION_WINDOW_SIZE_H = "/ui/window_height"
OPTION_MAIN_PANE_POS = "/ui/main_pane_position"
OPTION_SUB_PANE_POS = "/ui/sub_pane_position"
OPTION_WINDOW_MAX = "/ui/window_maximized"
OPTION_MAGNIFICATION = "/ui/text_magnification"
OPTION_PANE_LAYOUT = "/ui/pane_layout"
OPTION_OFFLINE = "/general/offline"
OPTION_POLL_FREQUENCY = "/general/poll_frequency"
OPTION_FEED_ID_SEQ = "feed_id_seq"
OPTION_FEEDS = "feeds"
OPTION_CATEGORIES = "categories"


# straw's home directory
def straw_home():
    home = os.getenv('HOME')
    if not home:
        home = os.path.expanduser('~')
    return os.path.join(home, '.straw')

def ensure_directory(strawdir):
    if os.path.exists(strawdir):
        if not os.path.isdir(strawdir):
            raise Exception
        return 0
    os.mkdir(strawdir)
    return 1

#############
# Config persistence classes
class ConfigPicklePersistence:
    def __init__(self, filename):
        self._config_file = filename
        self._dict = None

    def save_option(self, option, value):
        if self._dict is None:
            self._initialize_dict()
        temp_config_file = self._config_file + ".working"
        pickle_file = open(temp_config_file, "w")
        self._dict[option] = value
        cPickle.dump(self._dict, pickle_file, True)
        pickle_file.close()
        os.rename(temp_config_file, self._config_file)
        return

    def load_option(self, option):
        if self._dict is None:
            self._initialize_dict()
        return self._dict.get(option, None)

    def _initialize_dict(self):
        if os.path.exists(self._config_file):
            pickle_file = open(self._config_file, "r")
            self._dict = cPickle.load(pickle_file)
            pickle_file.close()
        else:
            self._dict = {}

class ConfigGConfPersistence:
    SAVERS = {OPTION_LAST_POLL: 'int',
              OPTION_ITEMS_STORED: 'int',
              OPTION_ITEM_ORDER: 'bool',
              OPTION_BROWSER_CMD: 'string',
              OPTION_WINDOW_SIZE_W: 'int',
              OPTION_WINDOW_SIZE_H: 'int',
              OPTION_MAIN_PANE_POS: 'int',
              OPTION_SUB_PANE_POS: 'int',
              OPTION_OFFLINE: 'bool',
              OPTION_WINDOW_MAX: 'bool',
              OPTION_MAGNIFICATION: 'float',
              OPTION_POLL_FREQUENCY: 'int',
              OPTION_PANE_LAYOUT: 'string', 
              }

    def __init__(self, client):
        self.client = client

    def save_option(self, option, value):
        getattr(self.client, 'set_' + self.SAVERS[option])(
            GCONF_STRAW_ROOT + option, value)

    def load_option(self, option):
        return getattr(self.client, 'get_' + self.SAVERS[option])(
            GCONF_STRAW_ROOT + option)

class ConfigPersistence:
    def __init__(self, *backends):
        self.backends = backends

    def save_option(self, option, value):
        for b in self.backends:
            if option in b[1]:
                b[0].save_option(option, value)

    def load_option(self, option):
        for b in self.backends:
            if option in b[1]:
                return b[0].load_option(option)

class ProxyConfig(object):
    def __init__(self):
        self._active = False
        self._host = None
        self._port = None
        self._auth = None
        self._username = None
        self._password = None

    @apply
    def active():
        doc = "this will be true when one of the subclasses is used"
        def fget(self):
            return self._active
        def fset(self, active):
            self._active = active
        return property(**locals())

    @apply
    def host():
        doc = "the host name where the proxy server lives"
        def fget(self):
            return self._host
        def fset(self, host):
            self._host = host
        return property(**locals())

    @apply
    def port():
        doc = "the port of the proxy server to connect to"
        def fget(self):
            return self._port
        def fset(self, port):
            self._port = port
        return property(**locals())

    @apply
    def auth():
        doc = "true if we need to authenticate to the proxy server"
        def fget(self):
            return self._auth
        def fset(self, isauth):
            self._auth = isauth
        return property(**locals())

    @apply
    def username():
        doc = "the username to use when authenticating to the proxy"
        def fget(self):
            return self._username
        def fset(self, username):
            self._username = username
        return property(**locals())

    @apply
    def password():
        doc = "the password to use when authenticating to the proxy"
        def fget(self):
            return self._password
        def fset(self, password):
            self._password = password
        return property(**locals())


class GconfProxyConfig(ProxyConfig):
    """Encapsulate proxy use and location information (host, port, ip),
    gconf reading and name lookup logic"""

    GCONF_HTTP_PROXY = "/system/http_proxy"
    GCONF_HTTP_PROXY_USE = GCONF_HTTP_PROXY + "/use_http_proxy"
    GCONF_HTTP_PROXY_HOST = GCONF_HTTP_PROXY + "/host"
    GCONF_HTTP_PROXY_PORT = GCONF_HTTP_PROXY + "/port"
    GCONF_HTTP_PROXY_AUTHENTICATION = GCONF_HTTP_PROXY + "/use_authentication"
    GCONF_HTTP_PROXY_USER = GCONF_HTTP_PROXY + "/authentication_user"
    GCONF_HTTP_PROXY_PASSWORD = GCONF_HTTP_PROXY + "/authentication_password"

    def __init__(self):
        ProxyConfig.__init__(self)
        client = gconf.client_get_default()
        self.active = client.dir_exists(self.GCONF_HTTP_PROXY)
        if not self.active:
            return

        client.add_dir(self.GCONF_HTTP_PROXY, gconf.CLIENT_PRELOAD_RECURSIVE)
        client.notify_add(self.GCONF_HTTP_PROXY_USE, self.proxy_mode_changed)
        client.notify_add(self.GCONF_HTTP_PROXY_HOST, self.proxy_host_changed)
        client.notify_add(self.GCONF_HTTP_PROXY_PORT, self.proxy_port_changed)
        client.notify_add(self.GCONF_HTTP_PROXY_AUTHENTICATION, self.proxy_auth_changed)
        client.notify_add(self.GCONF_HTTP_PROXY_USER, self.proxy_auth_username_changed)
        client.notify_add(self.GCONF_HTTP_PROXY_PASSWORD, self.proxy_auth_password_changed)

        self.active = client.get_bool(self.GCONF_HTTP_PROXY_USE)
        self.host = client.get_string(self.GCONF_HTTP_PROXY_HOST)
        self.port = client.get_int(self.GCONF_HTTP_PROXY_PORT)
        self.auth = client.get_bool(self.GCONF_HTTP_PROXY_AUTHENTICATION)
        if self.auth:
            self.username = client.get_string(self.GCONF_HTTP_PROXY_USER)
            self.password = client.get_string(self.GCONF_HTTP_PROXY_PASSWORD)


    # here be gconf logic
    def proxy_mode_changed(self, client, notify_id, entry, *args):
        self.active  = entry.value.get_bool()

    def proxy_host_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_string()
        if value:
            self.host = value
        else:
            self.active = False

    def proxy_port_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_int()
        if value:
            self.port = value
        else:
            self.active = False

    def proxy_auth_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_bool()
        if value:
            self.auth = value

    def proxy_auth_username_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_string()
        self.username = value

    def proxy_auth_password_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_string()
        self.password = value

class EnvironmentProxyConfig(ProxyConfig):
    """Encapsulate proxy use and location information, environment reading"""

    def __init__(self):
        ProxyConfig.__init__(self)
        self._read_env()

    def _read_env(self):
        proxy = None
        proxies = urllib.getproxies()
        if not proxies:
            return

        for key, value in proxies.iteritems():
            proxy = proxies.get(key)
        user, authority = urllib.splituser(proxy)
        if authority:
            self.host, self.port = urllib.splitport(authority)
            self.active = True
        if user:
            self.username, self.password = urllib.splitpasswd(user)
            self.auth = True
        return

class Config(gobject.GObject):
    _straw_config_file = os.path.join(straw_home(), "config")

    __gsignals__ = {
        'item-order-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'offline-mode-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'refresh-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'item-stored-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
        }

    def __init__(self, persistence):
        gobject.GObject.__init__(self)
        self.persistence = persistence
        self._feed_id_seq = 0
        self._poll_freq = 1800
        self._last_poll = 0
        self._browser_cmd = ''
        self._items_stored = 30
        self._item_order = True
        self._main_window_size = (640,480)
        self._main_pane_position = 100
        self._sub_pane_position = 100
        self.first_time = None
        self._offline = True
        self._window_maximized = False
        self._reload_css = False
        self._proxy = None
        self._pane_layout = 'vertical'

    def initialize_proxy(self):
        # EnvironmentProxy has precedence
        self._proxy = EnvironmentProxyConfig()
        if not self._proxy.active:
            # .. defaults to GConfProxyConfig so we can listen for network
            # setting changes (e.g, changes in network preference)
            self._proxy = GconfProxyConfig()

    def initialize_options(self):
        # Call this after calling Config's constructor
        self.first_time = ensure_directory(straw_home())

        if os.path.exists(self.straw_config_file):
            self._feed_id_seq = self.persistence.load_option(OPTION_FEED_ID_SEQ)

        self._poll_freq = self.persistence.load_option(OPTION_POLL_FREQUENCY)
        self._last_poll = self.persistence.load_option(OPTION_LAST_POLL)
        self._items_stored = self.persistence.load_option(OPTION_ITEMS_STORED)
        self._browser_cmd = self.persistence.load_option(OPTION_BROWSER_CMD)
        self._item_order = self.persistence.load_option(OPTION_ITEM_ORDER)

        if not self._poll_freq:
            self.poll_frequency = 1800
        if not self._items_stored:
            self.number_of_items_stored = 30
        if not self._item_order:
            self.item_order = True

        width = self.persistence.load_option(OPTION_WINDOW_SIZE_W)
        height = self.persistence.load_option(OPTION_WINDOW_SIZE_H)
        if not width:
            width = 640
        if not height:
            height = 480
        self._main_window_size = (width, height)

        self._main_pane_position = self.persistence.load_option(OPTION_MAIN_PANE_POS)
        self._sub_pane_position = self.persistence.load_option(OPTION_SUB_PANE_POS)
        if not self._main_pane_position:
            self._main_pane_position = 100
        if not self._sub_pane_position:
            self._sub_pane_position = 100
        print (self.main_pane_position, self.sub_pane_position)

        self._window_maximized = self.persistence.load_option(OPTION_WINDOW_MAX)
        self._text_magnification = self.persistence.load_option(OPTION_MAGNIFICATION)
        self._offline = self.persistence.load_option(OPTION_OFFLINE)
        self._pane_layout = self.persistence.load_option(OPTION_PANE_LAYOUT)

    @property
    def straw_config_file(self):
        return self._straw_config_file

    @property
    def proxy(self):
        return self._proxy

    @apply
    def feeds():
        doc = "Marshalled feed data"
        def fget(self):
            return self.persistence.load_option(OPTION_FEEDS)
        def fset(self, feeddata):
            self.persistence.save_option(OPTION_FEEDS, feeddata)
        return property(**locals())

    @apply
    def categories():
        doc = ""
        def fget(self):
            return self.persistence.load_option(OPTION_CATEGORIES)
        def fset(self, categorydata):
            self.persistence.save_option(
                OPTION_CATEGORIES, categorydata)
        return property(**locals())
    
    @apply
    def poll_frequency():
        doc = "Polling frequency"
        def fget(self):
            return self._poll_freq
        def fset(self, poll_frequency):
            if self._poll_freq != poll_frequency:
                self._poll_freq = poll_frequency
                self.persistence.save_option(OPTION_POLL_FREQUENCY, poll_frequency)
                self.emit('refresh-changed')
        return property(**locals())

    @apply
    def last_poll():
        doc = "last poll"
        def fget(self):
            return self._last_poll
        def fset(self, last_poll):
            if self._last_poll != last_poll:
                self._last_poll = last_poll
                self.persistence.save_option(OPTION_LAST_POLL, last_poll)
        return property(**locals())

    @apply
    def browser_cmd():
        doc = "The browser to use"
        def fget(self):
            return self._browser_cmd
        def fset(self, browser_cmd):
            if self._browser_cmd != browser_cmd:
                self._browser_cmd = browser_cmd
                self.persistence.save_option(OPTION_BROWSER_CMD, browser_cmd)
        return property(**locals())

    @apply
    def number_of_items_stored():
        doc = "Number of items to store per feed"
        def fget(self):
            return self._items_stored
        def fset(self, num=30):
            if self._items_stored != num:
                self._items_stored = num
                self.persistence.save_option(OPTION_ITEMS_STORED, num)
                self.emit('item-stored-changed')
        return property(**locals())

    @apply
    def item_order():
        doc = "Ordering of Items"
        def fget(self):
            return self._item_order
        def fset(self, order):
            if self._item_order != order:
                self._item_order = order
                self.persistence.save_option(OPTION_ITEM_ORDER, order)
                self.emit('item-order-changed')
        return property(**locals())

    @apply
    def feed_id_seq():
        doc = ""
        def fget(self):
            return self._feed_id_seq
        def fset(self, id):
            self._feed_id_seq = id
            self.persistence.save_option(OPTION_FEED_ID_SEQ, id)
        return property(**locals())

    def next_feed_id_seq(self):
        self.feed_id_seq += 1
        return self._feed_id_seq

    @apply
    def main_window_size():
        doc = ""
        def fget(self):
            return self._main_window_size
        def fset(self, size):
            if self._main_window_size != size:
                self._main_window_size = size
                self.persistence.save_option(OPTION_WINDOW_SIZE_W, size[0])
                self.persistence.save_option(OPTION_WINDOW_SIZE_H, size[1])
        return property(**locals())

    @apply
    def offline():
        doc = ""
        def fget(self):
            return self._offline
        def fset(self, mode):
            if self._offline != mode:
                self._offline = mode
                self.persistence.save_option(OPTION_OFFLINE, mode)
                self.emit('offline-mode-changed')
        return property(**locals())

    @apply
    def window_maximized():
        doc = ""
        def fget(self):
            return self._window_maximized
        def fset(self, state):
            if self._window_maximized is not state:
                self._window_maximized = state
                self.persistence.save_option(OPTION_WINDOW_MAX, state)
        return property(**locals())

    @apply
    def main_pane_position():
        doc = ""
        def fget(self):
            return self._main_pane_position
        def fset(self, position):
            if self._main_pane_position != position:
                self._main_pane_position = position
                self.persistence.save_option(OPTION_MAIN_PANE_POS, position)
        return property(**locals())

    @apply
    def sub_pane_position():
        doc = ""
        def fget(self):
            return self._sub_pane_position
        def fset(self, position):
            if self._sub_pane_position != position:
                self._sub_pane_position = position
                self.persistence.save_option(OPTION_SUB_PANE_POS, position)
        return property(**locals())
    
    @apply
    def pane_layout():
        doc = "The pane layout to use"
        def fget(self):
            return self._pane_layout
        def fset(self, pane_layout):
            if self._pane_layout != pane_layout:
                self._pane_layout = pane_layout
                self.persistence.save_option(OPTION_PANE_LAYOUT, pane_layout)
        return property(**locals())

    @apply
    def text_magnification():
        doc = "sets the amount of magnification in the item view"
        def fget(self):
            return self._text_magnification
        def fset(self, tm):
            if self._text_magnification is not tm:
                self._text_magnification = tm
                self.persistence.save_option(OPTION_MAGNIFICATION, tm)
        return property(**locals())

    @apply
    def reload_css():
        doc = ""
        def fget(self):
            return self._reload_css
        def fset(self, reload_css):
            self._reload_css = reload_css
        return property(**locals())

def convert_if_necessary(config):
    if not os.path.exists(config.straw_config_file):
        return

    try:
        f = open(config.straw_config_file, "r")
        cf = cPickle.load(f)

        if cf.has_key('poll_frequency'):
            config.poll_frequency = cf.get('poll_frequency')
            config.number_of_items_stored = cf.get('number_of_items_stored')
            config.item_order = cf.get('item_order')
            config.main_window_size = cf.get('main_window_size')
            config.main_pane_position = cf.get('main_pane_position')
            config.sub_pane_position = cf.get('sub_pane_position')
            config.offline = cf.get('offline')

            del cf['poll_frequency']
            del cf['number_of_items_stored']
            del cf['item_order']
            del cf['main_window_size']
            del cf['main_pane_position']
            del cf['sub_pane_position']
            del cf['offline']

            f.close()
            f = open(config.straw_config_file, "w")
            f.seek(0)
            f.truncate()
            cPickle.dump(cf, f, True)
    finally:
        f.close()
    return

def create_gconf_persistence():
    client = gconf.client_get_default()
    client.add_dir(GCONF_STRAW_ROOT, gconf.CLIENT_PRELOAD_ONELEVEL)
    return ConfigGConfPersistence(client)

def create_pickle_persistence():
    return ConfigPicklePersistence(Config._straw_config_file)

def create_instance():
    # Add your GConf key here as well!
    cp = ConfigPersistence(
        (create_gconf_persistence(),
         (OPTION_LAST_POLL, OPTION_ITEMS_STORED, OPTION_ITEM_ORDER, OPTION_BROWSER_CMD,
          OPTION_WINDOW_SIZE_W, OPTION_WINDOW_SIZE_H,
          OPTION_MAIN_PANE_POS, OPTION_SUB_PANE_POS, OPTION_OFFLINE,
          OPTION_MAGNIFICATION,
          OPTION_POLL_FREQUENCY, OPTION_WINDOW_MAX, OPTION_PANE_LAYOUT)),
        (create_pickle_persistence(),
         (OPTION_FEED_ID_SEQ, OPTION_FEEDS, OPTION_CATEGORIES)))
    config = Config(cp)
    config.initialize_proxy()
    config.initialize_options()
    convert_if_necessary(config)
    return config

config_instance = None
def get_instance():
    global config_instance
    if not config_instance:
        config_instance = create_instance()
    return config_instance
