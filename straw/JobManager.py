""" JobManager.py

Provides flexible infrastructure for handling various jobs from within the
application.

Very much WIP. Major TODO include:

- provide a way for registering observers
- handle emitting notification signals
- provide a method for getting JobInfo
- implement ThreadPoolJobHandler [in progress]

"""

__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from Queue import Queue, Empty
from gobject import GObject, SIGNAL_RUN_FIRST
from threading import Thread, Lock
import error
import gobject
import time

class JobManager(object):
    """
    Main entry point for the subsystem. JobManager is responsible managing
    JobHandlers, starting jobs, providing information about current status of
    a given job and notifying observers about lifecycle of a job.

    """

    def __init__(self):
        self.handlers = {}

    def register_handler(self, clazz):
        self.handlers[clazz.job_id] = clazz

    def start_job(self, job, wait = False):
        t = HandlerThread(self.handlers[job.job_id](job))
        t.start()

        if wait:
            t.join()

class Job(object):
    def __init__(self, job_id):
        self.job_id = job_id
        self.data = None
        self.observers = None

class JobHandler(GObject):
    """
    This class represents an abstract JobHandler which provides common functionality
    used by all handlers. Each job handler is created with given Job to do.

    """

    __gsignals__ = {
                    'job-done' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
                    'task-done' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
                    'task-start' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,))
                    }

    def __init__(self, job):
        gobject.GObject.__init__(self)
        self.job = job
        self.task_queue = Queue()
        self.result_queue = Queue()

        if job.observers != None:
            for observer_dict in job.observers:
                for signal in observer_dict.keys():
                    for callable in observer_dict[signal]:
                        self.connect(signal, callable)

    def start(self):
        self._split()
        self._run()

    def _split(self):
        raise NotImplementedError

    def _run(self):
        raise NotImplementedError

    def _notify(self, event, data):
        self.emit(event, data)

    def _post_result(self, task, result):
        self.result_queue.push(result)

    def _prepare_result(self):
        return self.result_queue

class HandlerThread(Thread):
    def __init__(self, handler):
        self.handler = handler
        Thread.__init__(self)

    def run(self):
        self.handler.start()

class ThreadPoolJobHandler(JobHandler):
    """
    This handler uses a thread pool to efficiently process the data in
    non-blocking manner. Useful for jobs that need to be done in the
    background.

    """

    def __init__(self, job):
        JobHandler.__init__(self, job)

        self.pool_size = 3
        self.task_threads = []
        self.task_class = None
        self.queue_lock = Lock()

    def _run(self):
        for i in xrange(self.pool_size):
            t = self.task_class(self, self.task_queue, self.result_queue)
            t.setName(str(i))
            t.start()
            self.task_threads.append(t)

        error.debug("pool -- created threads, now waiting on the queue")

        self.task_queue.join()

        error.debug("pool -- work is finished, wait for workers")

        for t in self.task_threads:
            t.job_done()
            t.join()

        error.debug("pool -- done, sending job-done")

        self._notify("job-done", self._prepare_result())

class TaskInfo:
    def __init__(self, id, data):
        self.id = id
        self.data = data

class TaskResult:
    def __init__(self, task_info, result):
        self.task_info = task_info
        self.result = result

    def get(self):
        raise NotImplementedError

class TaskThread(Thread):
    def __init__(self, handler, task_queue, result_queue):
        self.handler = handler
        self.task_queue = task_queue
        self.result_queue = result_queue
        self.quit = False

        Thread.__init__(self)

    def run(self):
        while 1:
            error.debug(self.getName() + " trying to acquire lock!")
            self.handler.queue_lock.acquire()
            error.debug(self.getName() + " acquired lock!") 
            
            if self.quit or self.task_queue.empty():
                self.handler.queue_lock.release()
                break

            task = None
            
            try:
                task = self.task_queue.get_nowait()
            except Empty:
                error.debug(self.getName() + " missed the party!")

            self.handler.queue_lock.release()
            error.debug(self.getName() + " released lock!")

            if task:
                error.debug(self.getName() + " is munching task with id = " + str(task.id))
                self._task_started(task)
                result = self._process(task)
                self._post_result(task, result)
                error.debug(self.getName() + " is posting result to task with id = " + str(task.id))
                self.task_queue.task_done()

    def job_done(self):
        self.quit = True

    def _post_result(self, task, result):
        task_result = TaskResult(task, result)
        self.result_queue.put_nowait(task_result)
        self.handler._notify("task-done", task_result)
    
    def _prepare_task_info(self, task_info):
        return task_info

    def _task_started(self, task):
        self.handler._notify("task-start", self._prepare_task_info(task))

    def _process(self, task):
        raise NotImplementedError

_job_manager = None

def _get_instance():
    global _job_manager
    if not _job_manager:
        _job_manager = JobManager()
    return _job_manager

def register_handler(clazz):
    _get_instance().register_handler(clazz)
    
def start_job(job, wait = False):
    _get_instance().start_job(job, wait)
