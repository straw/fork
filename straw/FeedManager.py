from JobManager import Job
from gobject import GObject, SIGNAL_RUN_FIRST
from model import Feed, Item, Category
from storage import DAO, Storage
import FeedUpdater
import JobManager
import gobject
import straw

_storage_path = None

model_data = None

def import_opml(path, category):
    fm = _get_instance()
    fm.import_opml(path, category)

def init():
    fm = _get_instance()
    fm.init_storage(_storage_path)

def setup(storage_path = None):
    global _storage_path
    _storage_path = storage_path

def lookup_feed(id, ctg_id):
    fm = _get_instance()
    return fm.lookup_feed(id, ctg_id)

def lookup_category(ctg_id):
    fm = _get_instance()
    return fm.lookup_category(ctg_id)

def get_feed_items(id):
    fm = _get_instance()
    return fm.get_feed_items(id)

def get_category_items(id):
    fm = _get_instance()
    return fm.get_category_items(id)

def get_feeds():
    fm = _get_instance()
    return fm.get_feeds()

def update_all_feeds(observers):
    fm = _get_instance()
    return fm.update_all_feeds(observers)

def save_category(title, pid = None):
    fm = _get_instance()
    return fm.save_category(title, parent_id = pid)

def save_feed(feed):
    fm = _get_instance()
    result = fm.save_feed(feed)
    return result

def save_all(save_list):
    fm = _get_instance()
    fm.save_all(save_list)

def get_model():
    fm = _get_instance()
    return fm.categories, fm.model

def categories(root_id = 1):
    _categories, model = get_model()

    yield _categories[root_id]

    if model.has_key(root_id):
        for node in model[root_id]:
            if node.type == "C":
                for child in categories(node.id):
                    yield child
    return

def move_node(node, new_parent, new_order):
    fm = _get_instance()
    fm.move_node(node, new_parent, new_order)

class FeedManager(GObject):
    __gsignals__ = {
        "item-added" :     (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "feed-added" :     (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "category-added" :     (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
    }

    def __init__(self):
        GObject.__init__(self)

        self.storage = None

    def init_storage(self, path):
        self.storage = Storage(path)
        self.dao = DAO(self.storage)
        self.nodes, self.model = self.dao.get_nodes()

        self._process_model()

    def import_opml(self, path, category):
        job = Job("opml-parse")
        job.data = (path, category)
        job.observers = [ { "job-done": [ self.import_opml_save ] } ]

        JobManager.start_job(job)

    def import_opml_save(self, handler, opml_data):
        save_list, category = opml_data

        self.dao.tx_begin()
        self.save_all(save_list)
        self.dao.tx_commit()

    def _model_add_node(self, node):
        if not self.model.has_key(node.parent_id):
            self.model[node.parent_id] = []

        self.model[node.parent_id].append(node)

    def _calculate_order(self, node):
        if not self.model.has_key(node.parent.id):
            return 0
        else:
            return len(self.model[node.parent.id])
    
    def move_node(self, node, new_parent, new_order):
        print node
        print new_parent
        print new_order

    def save_all(self, save_list):
        for item in save_list:
            if isinstance(item, Feed):
                item.parent_id = 1

                if item.parent:
                    item.parent_id = item.parent.id
                else:
                    item.parent_id = 1
                    item.parent = self.nodes[1]

                item.norder = self._calculate_order(item)

                self.save_feed(item)
            else:
                if item.parent:
                    item.parent_id = item.parent.id
                else:
                    item.parent_id = 1
                    item.parent = self.nodes[1]

                item.norder = self._calculate_order(item)

                self.save_category(item)

    def lookup_feed(self, id):
        return self.nodes[id]

    def lookup_category(self, id):
        return self.categories[id]

    def save_feed(self, feed):
        if not feed.parent_id:
            feed.parent_id = 1

        category = self.lookup_category(feed.parent_id)

        if not category:
            return None

        result = self.dao.save(feed)

        if not self.feeds.has_key(feed.parent_id):
            self.feeds[feed.parent_id] = []

        feed.connect("unread-count-changed", category.on_unread_count_changed)

        self.feeds[feed.parent_id].append(feed)
        self.nodes[feed.id] = feed
        self._model_add_node(feed)

        self.emit("feed-added", feed)

        return result

    def update_all_feeds(self, observers):
        feeds = [node for key in self.model.keys() for node in self.model[key] if node.type == "F"]

        FeedUpdater.update_feeds(feeds, [{
                                         "job-done": [ self.update_all_feeds_done ],
                                         "task-start": [ self._on_update_feed_start ],
                                         "task-done": [ self._on_update_feed_done ]
                                         }, observers])

    def update_all_feeds_done(self, handler, data):
        pass

    def _on_update_feed_start(self, handler, feed):
        feed.props.status = straw.FS_UPDATING

    def _on_update_feed_done(self, handler, data):
        feed = data.task_info.data["feed"]
        feed.props.status = straw.FS_IDLE

        if not data.result:
            return

        for item in data.result.items:
            item.feed_id = feed.id

            if not self.feed_item_exists(item):
                self.dao.save(item)
                feed.props.unread_count += 1
                self.emit("item-added", item)

    ### CATEGORIES

    def save_category(self, category):
        self.dao.save(category)

        category.connect("unread-count-changed",
                                 self.lookup_category(category.parent_id).on_unread_count_changed)

        self.categories[category.id] = category
        self.nodes[category.id] = category
        self._model_add_node(category)

        self.emit("category-added", category)

    # ITEMS

    def get_feed_items(self, id):
        feed = self.lookup_feed(id)
        feed.items = self.dao.get(Item, " WHERE feed_id = %d" % feed.id)
        for item in feed.items:
            item.feed = feed
            item.connect("is-read-changed", feed.on_is_read_changed)
            item.connect("is-read-changed", self.on_item_is_read_changed)
        return feed.items

    def get_category_items(self, id):
        category = self.lookup_category(id)

        if category == None:
            return None

        def _get_children_feeds(category_id):
            feed_ids = []

            if self.feeds.has_key(category_id):
                feed_ids = [str(feed.id) for feed in self.feeds[category_id]]

            children_categories = [ctg_id for ctg_id in self.categories.keys() if self.categories[ctg_id].parent_id == category_id]

            for ctg_id in children_categories:
                feed_ids.extend(_get_children_feeds(ctg_id))

            return feed_ids

        in_str = ", ".join(_get_children_feeds(id))

        items = self.dao.get(Item, " WHERE feed_id IN(%s) ORDER BY id" % in_str)

        for item in items:
            feed = self.lookup_feed(item.feed_id)
            item.feed = feed
            item.connect("is-read-changed", feed.on_is_read_changed)
            item.connect("is-read-changed", self.on_item_is_read_changed)

        return items

    def feed_item_exists(self, item):
        result = self.dao.get(Item, " WHERE feed_id = ? AND title = ?", (item.feed_id, item.title,))
        return len(result) > 0

    def on_item_is_read_changed(self, obj, is_read):
        self.dao.save(obj)

    def _process_model(self):
        self.categories = dict([(node.id, node) for node in self.nodes.values() if node.type == "C"])
        self.feeds = {}

        for category_id in self.categories.keys():
            category = self.categories[category_id]

            if category.parent_id != None:
                category.connect("unread-count-changed",
                                 self.categories[category.parent_id].on_unread_count_changed)

        for node in self.nodes.values():
            if node.parent_id != None:
                node.parent = self.nodes[node.parent_id]
            else:
                node.parent = None

            if node.type == "F":
                if not self.feeds.has_key(node.parent_id):
                    self.feeds[node.parent_id] = []

                self.feeds[node.parent_id].append(node)
                self.categories[node.parent_id].props.unread_count += node.unread_count

                node.connect("unread-count-changed", node.parent.on_unread_count_changed)

_instance = None

def _get_instance():
    global _instance
    if not _instance:
        _instance = FeedManager()
    return _instance
