""" preferences.py

Module setting user preferences.
"""
__copyright__ = """
Copyright (c) 2002-2004 Juri Pakaste
Copyright (c) 2005-2007 Straw Contributors
"""
__license__ = """ GNU General Public License

Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

import Config
import gtk
from gtk.glade import XML

import os
from os.path import join

import straw

class Preferences:
    SEC_PER_MINUTE = 60

    def __init__(self):
        config = Config.get_instance()
        self._glade = XML(join(straw.STRAW_DATA_DIR,'preferences.glade'))
        self._window = self._glade.get_widget('preferences')
        self._browser_override = self._glade.get_widget("prefs_browser_setting")
        self._browser_entry = self._glade.get_widget("prefs_browser_setting_entry")

        # General
        config = Config.get_instance()
        poll_frequency = int(config.poll_frequency/self.SEC_PER_MINUTE)
        items_stored = int(config.number_of_items_stored)

        browser_cmd = config.browser_cmd
        if browser_cmd:
            self._browser_override.set_active(True)
            self._browser_entry.set_text (browser_cmd)
            self._browser_entry.set_sensitive (True)
        else:
            self._browser_entry.set_text (_("Using desktop setting"))

        self._glade.get_widget('poll_frequency_spin').set_value(poll_frequency)
        self._glade.get_widget('number_of_items_spin').set_value(items_stored)
        self._glade.get_widget(['item_order_oldest',
                        'item_order_newest'][config.item_order]).set_active(1)
        self._glade.get_widget('vertical_layout').set_active(config.pane_layout == 'vertical')

        nameFuncMap = {}
        for key in dir(self.__class__):
            if key[:3] == 'on_':
                nameFuncMap[key] = getattr(self, key)
        self._glade.signal_autoconnect(nameFuncMap)

    def show(self, *args):
        self._window.present()

    def hide(self, *args):
        self._window.hide()

    def on_preferences_delete_event(self, *args):
        self.hide()
        return True

    def on_close_button_clicked(self, button):
        self.hide()
        return

    def on_poll_frequency_spin_value_changed(self, spin):
        Config.get_instance().poll_frequency = int(spin.get_value() * self.SEC_PER_MINUTE)

    def on_number_of_items_spin_value_changed(self, spin):
        Config.get_instance().number_of_items_stored = int(spin.get_value())

    def on_item_order_newest_toggled(self, radio):
        config = Config.get_instance()
        config.item_order = not config.item_order

    def on_item_order_oldest_toggled(self, radio):
        pass

    def on_prefs_browser_setting_toggled(self, button):
        active = button.get_active()
        if not active:
            config = Config.get_instance()
            config.browser_cmd = ""
        self._browser_entry.set_sensitive(active)

    def on_prefs_browser_setting_entry_focus_out_event(self, entry, *args):
        config = Config.get_instance()
        config.browser_cmd = entry.get_text()

    def on_vertical_layout_clicked(self, *args):
        print 'vert clicked'
        config = Config.get_instance()
        config.pane_layout = 'vertical'

    def on_horizontal_layout_clicked(self, *args):
        print 'orintal clicked'
        config = Config.get_instance()
        config.pane_layout = 'horizontal'


_dialog = None
def preferences_show():
    global _dialog
    if not _dialog:
        _dialog = Preferences()
    _dialog.show()
