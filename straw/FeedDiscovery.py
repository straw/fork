"""

"""

__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from JobManager import Job, TaskResult, TaskThread, TaskInfo, ThreadPoolJobHandler
import JobManager
import SummaryParser
import feedfinder

class FeedDiscoveryJobHandler(ThreadPoolJobHandler):
    job_id = "feed-discovery"

    def __init__(self, job):
        ThreadPoolJobHandler.__init__(self, job)

        self.pool_size = 1
        self.result_class = FeedDiscoveryTaskResult
        self.task_class = FeedDiscoveryTaskThread

    def _split(self):
        ti = TaskInfo(1, { "url" : self.job.data })
        self.task_queue.put(ti)

    def _prepare_result(self):
        task_result = self.result_queue.get()
        return task_result.result

class FeedDiscoveryTaskResult(TaskResult):
    def __init__(self, task_info, result):
        self.task_info = task_info
        self.result = result

    def get(self):
        raise NotImplementedError

class FeedDiscoveryTaskThread(TaskThread):
    def __init__(self, handler, task_queue, result_queue):
        TaskThread.__init__(self, handler, task_queue, result_queue)

    def _process(self, task):
        url = task.data["url"]
        data = feedfinder.feeds(url, True)
        feeds = [SummaryParser.parse(content, location = url) for url, content in data]
        return feeds

JobManager.register_handler(FeedDiscoveryJobHandler)

def discover(url, observers):
    update = Job("feed-discovery")
    update.data = url
    update.observers = observers
    JobManager.start_job(update)
