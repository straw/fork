CREATE TABLE IF NOT EXISTS nodes (
    id INTEGER PRIMARY KEY,
    parent_id INTEGER,
    type VARCHAR(1) NOT NULL,
    norder INTEGER NOT NULL
);
--
CREATE TABLE IF NOT EXISTS categories (
    id INTEGER NOT NULL,
    name TEXT NOT NULL
);
--
CREATE TABLE IF NOT EXISTS feeds (
    id INTEGER NOT NULL,
    title TEXT,
    location VARCHAR(255) NOT NULL,
    link VARCHAR(255)
);
--
CREATE TABLE IF NOT EXISTS items (
    id INTEGER PRIMARY KEY,
    title TEXT NOT NULL,
    feed_id INTEGER NOT NULL,
    is_read INTEGER NOT NULL DEFAULT 0,
    link TEXT,
    pub_date TIMESTAMP NOT NULL,
    description TEXT NOT NULL
);
--
CREATE INDEX idx_nodes_id ON nodes (id)
--
CREATE INDEX idx_feeds_id ON feeds (id)
--
CREATE INDEX idx_categories_id ON categories (id)
--
CREATE INDEX idx_items_feed_id ON items (feed_id)
--
INSERT INTO nodes (id, parent_id, type, norder) VALUES (1, NULL, 'C', 0);
--
INSERT INTO categories (id, name) VALUES (1, 'root');
