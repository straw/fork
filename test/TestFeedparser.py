""" Feedparser threading problem testcase

This is a simple (non-scientific :P) testcase that helps reproduce the problem we
encountered when developing Straw feed reader.

The problem probably isn't specific to feedparser, but rather to SAX parser or
sgmllib (with both this problem occurs) that feedparser uses.

To run the testcase, enter the main Straw directory and execute the following command:

:~ nosetests -s test/TestFeedparser.py

Note: if using CountingThread, it's best to redirect test output to a file. Like this:

:~ nosetests -s test/TestFeedparser.py > result.txt

Description of this testcase:

- there are sample feeds in TEST_DATA_PATH directory
- testcase passes these feeds to feedparser's parse method
- before than testcase launches either GUIThread (Straw's main window from Glade file)
  or CountingThread (simple thread that counts :-)

To observe the problem, do the following:

- GUIThread - open File menu and hold arrow down key - selection in the menu should
  cycle smoothly, but it freezes from time to time - this effect can be also observed
  in other parts of the GUI

- CountingThread - set some interval (default is 0.05), dump the counting results into
  a file and observe that delays are often more than 0.1 or the other interval that has
  been set

It is possible to control test CPU load by modifying sleep interval in parsing loop.

Also try uncommenting the line "while 1: pass". In that case, CPU load is constantly at
100%, but the background thread is working smoothly.

If you have any ideas on what might be the cause of this problem, please come to #straw
at irc.freenode.net and kindly enlighten us ;-)

"""

from gtk import glade
from straw import feedparser
from threading import Thread
import gtk
import os
import time

TEST_DATA_PATH = "test/feeds/"

class TestFeedparser(object):
    def setUp(self):
        pass 

    def tearDown(self):
        pass
    
    def testPerformance(self):
        gtk.gdk.threads_init()
        gtk.gdk.threads_enter()

        t = CountingThread()
        #t = GUIThread()

        t.start()

        while 1: pass

        for file in os.listdir(TEST_DATA_PATH):
            #time.sleep(0.1)
            fl = open(TEST_DATA_PATH + file, "r")
            content = fl.read()
            fl.close()

            feedparser.parse(content)

        t.join()

        gtk.gdk.threads_leave()

class GUIThread(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        xml = glade.XML('data/straw.glade', "straw_main")
        window = xml.get_widget('straw_main')
        window.show_all()
        gtk.main()

class CountingThread(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        while 1:
            time.sleep(0.05)
            print time.time()
