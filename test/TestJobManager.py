import pygtk
import gobject
from mock import Mock
import nose

from straw.FeedUpdater import FeedUpdateJobHandler
from straw.JobManager import SimpleJobHandler, TestThreadPoolJobHandler
import straw.JobManager as JobManager

class TestJobManager:
    def testSimpleJobHandler(self):
        JobManager.register_handler(SimpleJobHandler)
        JobManager.start_job("multiply-by-2", [1, 2, 3])

    def testThreadPoolJobHandler(self):
        JobManager.register_handler(TestThreadPoolJobHandler)
        a = []
        for i in xrange(10):
            a.append(i)
        #JobManager.start_job("some-job", a, True)

    def testFeedUpdateJobHandler(self):
        JobManager.register_handler(FeedUpdateJobHandler)
        a = [ "http://rss.slashdot.org/Slashdot/slashdot", \
             "http://newsrss.bbc.co.uk/rss/sportonline_world_edition/tennis/rss.xml", \
             "http://news.com.com/2547-1_3-0-5.xml", \
             "http://feeds.wired.com/wired/topheadlines", \
             "http://www.theregister.co.uk/headlines.rss", \
             "http://feeds.feedburner.com/boingboing/iBag" ]

        JobManager.start_job("feed-update", a, True)