
import pygtk
import gobject
from mock import Mock
import nose

from straw.SummaryItem import SummaryItem

class TestSummaryItem:
    def setUp(self):
        mock = Mock( {
                'add_refer': None,
                '__nonzero__' : 1
                })
        self.summaryItem = SummaryItem(mock)

    def tearDown(self):
        self.summaryItem = None
        mock = None

    def test_seen(self):
        def _cb(*args): assert True
        self.summaryItem.connect('read', _cb)
        self.summaryItem.seen = True
        assert self.summaryItem.seen is True

    def test_sticky(self):
        def _cb(*args): assert True
        self.summaryItem.connect('sticky', _cb)
        self.summaryItem.sticky = True
        assert self.summaryItem.sticky is True
        self.summaryItem.sticky = False
        assert self.summaryItem.sticky is False

    def test_id(self):
        self.summaryItem.id = 1
        assert summaryItem.id is 1
